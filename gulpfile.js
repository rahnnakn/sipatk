var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

var dirs = {
    'resources/assets/sass/materialize.css': 'public/assets/css/materialize.css',
    'resources/assets/sass/page-center.css': 'public/assets/css/page-center.css',
    'resources/assets/sass/prism.css': 'public/assets/css/prism.css',
    'resources/assets/sass/style.css': 'public/assets/css/style.css',
    'resources/assets/js/jquery-validation.min.js': 'public/assets/js/jquery-validation.min.js',
    'resources/assets/js/jquery.min.js': 'public/assets/js/jquery.min.js',
    'resources/assets/js/materialize.js': 'public/assets/js/materialize.js',
    'resources/assets/js/materialize.min.js': 'public/assets/js/materialize.min.js',
    'resources/assets/js/plugins': 'public/assets/js/plugins',
    'resources/assets/js/plugins.js': 'public/assets/js/plugins.js',
    'resources/assets/js/prism.js': 'public/assets/js/prism.js',
    'resources/assets/js/raphael-min.js': 'public/assets/js/raphael-min.js'
}

elixir(function(mix) {
    mix.sass('app.scss', 'public/assets/css/app.css')
    	.scripts('resources/assets/js/app.js', 'public/assets/js/app.js');
    for (dir in dirs)
        mix.copy(dir, dirs[dir]);
});
