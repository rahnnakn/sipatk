<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryRequest extends Model
{
    protected $table = 'requests';
    public $incrementing = false;

    protected $fillable = [
        'author',
        'division',
        'approver',
        'receiver',
        'purpose',
    ];

    public function u_sender() 
    {
    	return $this->belongsTo(User::class, 'author');
    }

    public function u_receiver() 
    {
    	return $this->belongsTo(User::class, 'receiver');
    }

    public function u_approver()
    {
        return $this->belongsTo(User::class, 'approver');
    }

    public function inventories() 
    {
    	return $this->hasMany(RequestedInventory::class, 'request_id');
    }

    public function bins() 
    {
    	return $this->hasMany(BinRequest::class);
    }
}
