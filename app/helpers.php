<?php

use App\Inventory;
use App\InventoryCard;
use App\InventoryRequest;
use App\InventoryCategory;
use App\BinInventory;
use App\BinInventoryCategory;
use App\BinRequest;
use App\BinRequestedInventory;
use App\BinUser;
use App\User;

/*
 * Returning the currently logged in person information
 */
function whoami()
{
    if (User::find(session()->get('username')) != null) {
        $u = User::find(session()->get('username'));
        return $u;
    }
}

/*
 * Check if the currently logged in person is an operator
 */
function operator()
{
    if (User::find(session()->get('username')) != null) {
        $a = User::find(session()->get('username'))->isOperator;
        return $a;
    }
}

/*
 * Check if the currently logged in person is a manager
 */
function manager()
{
    if (User::find(session()->get('username')) != null) {
        $a = User::find(session()->get('username'))->isManager;
        return $a;
    }
}

/*
 * Count new requests for each respective role
 */
function count_requests()
{
    if (operator()) {
        $c = InventoryRequest::where('isShown', 1)
                ->where('status', 3)
                ->get()
                ->count();
        return $c;
    } else if (manager() && (session()->get('username') == 'baharipri' || session()->get('username') == 'marta')) {
        $c = InventoryRequest::where('isShown', 1)
                ->where('status', 2)
                ->get()
                ->count();
        return $c;
    } else {
        $c = InventoryRequest::where('isShown', 1)
                ->where('status', 1)
                ->where('approver', session()->get('username'))
                ->get()
                ->count();
        return $c;
    }
    return redirect()->route('login');
}

/*
 * Get Current Stock
 */
function get_stock($cards)
{
    // initiate overall stock with 0
    $current_stock = 0;
    // then for each row of card
    foreach ($cards as $card) {
        // start from the earliest transaction
        // count stock for each card with (in - out), might get minus
        $stock_per_card = $card->in - $card->out; 
        // then associate it with the overall stock
        $current_stock += $stock_per_card;
        // update this stock of card with overall stock
        InventoryCard::where('created_at', $card->created_at)
                        ->update(['stock' => $current_stock]);
    }
}

/*
 * Create New Bin for Inventory Category
 */
function new_binInventoryCategory($cat)
{
    $new_bin = new BinInventoryCategory;
    $new_bin->id = $cat->id;
    $new_bin->name = $cat->name;
    $new_bin->description = $cat->description;
    $new_bin->created_at = $cat->created_at;
    $new_bin->updated_at = $cat->updated_at;
    $new_bin->save();
}

/*
 * Create New Bin for Inventory
 */
function new_binInventory($inventory)
{
    $new_bin = new BinInventory;
    $new_bin->id = $inventory->id;
    $new_bin->name = $inventory->name;
    $new_bin->status = $inventory->status;
    $new_bin->min_stock = $inventory->min_stock;
    $new_bin->category = $inventory->category;
    $new_bin->created_at = $inventory->created_at;
    $new_bin->updated_at = $inventory->updated_at;
    $new_bin->save();
}

/*
 * Create New Bin for User
 */
function new_binUser($user)
{
    $new_user = new BinUser;
    $new_user->nip = $user->nip;
    $new_user->username = $user->username;
    $new_user->name = $user->name;
    $new_user->email = $user->email;
    $new_user->division = $user->division;
    $new_user->isOperator = $user->isOperator;
    $new_user->isManager = $user->isManager;
    $new_user->created_at = $user->created_at;
    $new_user->updated_at = $user->updated_at;
    $new_user->save();
}

/*
 * Create New Bin for Request
 */
function new_binRequest($request)
{
    $new_bin = new BinRequest;
    $new_bin->id = $request->id;
    $new_bin->author = $request->author;
    $new_bin->status = $request->status;
    $new_bin->purpose = $request->purpose;
    $new_bin->approver = $request->approver;
    if ($request->receiver != null) {
        $new_bin->receiver = $request->receiver;
    }
    $new_bin->admin_note = $request->admin_note;
    $new_bin->created_at = $request->created_at;
    $new_bin->updated_at = $request->updated_at;
    $new_bin->save();
}

/*
 * Create New Bin for Requested Inventory
 */
function new_binRequestedInventory($request)
{
    $r_inventories = $request->inventories()->get();
    foreach ($r_inventories as $r_inventory) {
        $new_bin = new BinRequestedInventory;
        $new_bin->id = $r_inventory->id;
        $new_bin->request_id = $r_inventory->request_id;
        $new_bin->inventory_id = $r_inventory->inventory_id;
        $new_bin->quantity = $r_inventory->quantity;
        $new_bin->unit = $r_inventory->unit;
        $new_bin->save();
    }
}

/*
 * Create New Cards for Finished Request
 */
function new_cards($r_inventories, $a_request)
{
    foreach ($r_inventories as $r_inventory) {
        $inventory = Inventory::find($r_inventory->inventory_id);
        $card = new InventoryCard;
        $card->inventory_id = $r_inventory->inventory_id;
        $card->transaction_date = $a_request->updated_at->format('Y-m-d');
        $card->bill_num = 'Permintaan';
        $card->from = 'SLA';
        $card->to = $a_request->u_sender->name;
        $card->in = 0;
        $card->out = $r_inventory->quantity;
        $card->stock = 0;
        $card->isShown = 1;
        $card->save();

        $cards = InventoryCard::where('inventory_id', $r_inventory->inventory_id)->orderBy('transaction_date', 'asc')->get();
        get_stock($cards);

        set_inventoryStatus($card, $inventory);
    }
}

/*
 * Create A Card for New Inventory
 */
function set_defaultCard($from, $to, $in, $out)
{
    if ($from == "")
        return $from = "-";
    if ($to == "")
        return $to = "-";
    if ($in == "")
        return $in = 0;
    if ($out == "")
        return $out = 0;
}

/*
 * Set Inventory Status on Update
 */
function set_inventoryStatus($new_card, $inventory)
{
    if ($new_card->stock <= $inventory->min_stock) {
        if ($inventory->status == 1) {
            new_binInventory($inventory, $inventory->id);

            Inventory::where('id', $inventory->id)->update([
                'status' => 0,
            ]);
        }
    } else {
        if ($inventory->status == 0) {
            new_binInventory($inventory, $inventory->id);

            Inventory::where('id', $inventory->id)->update([
                'status' => 1,
            ]);
        }
    }
}

/*
 * Do a Validation for A Request
 */
function validation_request($input)
{
    $validator = [];
    $i = 0;
    if (isset($input['purpose']) && isset($input['approver'])) {
        $validator[$i] = Validator::make($input, [
                'purpose' => 'required',
                'approver' => 'required|exists:users,username',
        ]);
        $i++;
    }
    if (isset($input['inventory_name'])) {
        $inventories_name = $input['inventory_name'];
        $validator[$i] = Validator::make($inventories_name, [
            'required|exists:inventories,id',
        ]);
        $i++;
    }
    if (isset($input['quantity'])) {
        $inventories_quantity = $input['quantity'];
        $validator[$i] = Validator::make($inventories_quantity, [
            'required|integer',
        ]);
        $i++;
    }
    if (isset($input['unit'])) {
        $inventories_unit = $input['unit'];
        $validator[$i] = Validator::make($inventories_unit, [
            'required|exists:unit_quantities,unit',
        ]);
    }
    $hasFailed = false;

    foreach ($validator as $a_validator) {
        $hasFailed = $a_validator->fails() || $hasFailed;
    }

    return $hasFailed;
}

/*
 * Get a respond text for certain request status
 */
function get_textStatus($request) {
    $outputs = [
        ['status' => 1, 'text' => 'Permintaan ini belum disetujui kepala divisi.'],
        ['status' => 2, 'text' => 'Permintaan ini telah disetujui kepala divisi.'],
        ['status' => 3, 'text' => 'Permintaan ini telah disetujui kepala SLA.'],
        ['status' => 4, 'text' => 'Permintaan ini sedang diproses.'],
        ['status' => 5, 'text' => 'Permintaan ini telah selesai diproses.'],
    ];

    return array_get($outputs, 'status.'.$request->status);    
}

/*
 * Check if the needed status of a request is true
 */
function check_request_status($request, $requiredStatus) {
    if ($request->status == $requiredStatus) { 
        return true;  
    }
    return false;
}

/*
 * Get the amount of coming for this inventory per month
 */
function in_per_month($month, $year, $id) {
    $total = 0;
    $transactions = InventoryCard::where('isShown', 1)
                    ->where('inventory_id', $id)
                    ->whereYear('transaction_date', '=', $year)
                    ->whereMonth('transaction_date', '=', $month)
                    ->get();
    if (!is_null($transactions)) {
        foreach ($transactions as $transaction) {
            $total += $transaction->in;
        }
    }
    return $total;
}

/*
 * Get the amount of leaving for this inventory per month
 */
function out_per_month($month, $year, $id) {
    $total = 0;
    $transactions = InventoryCard::where('isShown', 1)
                    ->where('inventory_id', $id)
                    ->whereYear('transaction_date', '=', $year)
                    ->whereMonth('transaction_date', '=', $month)
                    ->get();
    if (!is_null($transactions)) {
        foreach ($transactions as $transaction) {
            $total += $transaction->out;
        }
    }
    return $total;
}

/*
 * Get the total of coming for this inventory per year
 */
function in_total($year, $id) {
    $month = 12;
    $total = 0;
    for ($i = 1; $i <= 12; $i++) {
        $total += in_per_month($i, $year, $id);
    }
    return $total;
}

/*
 * Get the total of leaving for this inventory per year
 */
function out_total($year, $id) {
    $month = 12;
    $total = 0;
    for ($i = 1; $i <= 12; $i++) {
        $total += out_per_month($i, $year, $id);
    }
    return $total;
}