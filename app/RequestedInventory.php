<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestedInventory extends Model
{
    public $incrementing = false;
    protected $table = 'requested_inventories';

    protected $fillable = [
        'request_id',
        'inventory_id',
        'quantity',
        'unit',
    ];

    public function inventory() 
    {
    	return $this->belongsTo(Inventory::class, 'inventory_id');
    }

    public function request() 
    {
    	return $this->belongsTo(InventoryRequest::class, 'request_id');
    }

    public function bins()
    {
        return $this->hasMany(BinRequestedInventory::class);
    }
}
