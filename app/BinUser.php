<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BinUser extends Model
{
    protected $table = 'bin_users';

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
