<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryCategory extends Model
{
    protected $table = 'inventory_categories';

    protected $fillable = [
        'name',
        'description',
        'isShown',
    ];

    public function inventory() 
    {
    	return $this->hasMany(Inventory::class);
    }
}
