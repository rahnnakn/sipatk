<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BinInventory extends Model
{
	protected $table = 'bin_inventories';
    public $incrementing = false;

    public function inventory()
    {
        return $this->belongsTo(Inventory::class);
    }
}
