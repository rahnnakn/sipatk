<?php

namespace App\Http\Controllers;

use View;
use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\BinRequest;
use App\Division;
use App\Inventory;
use App\InventoryRequest;
use App\RequestedInventory;
use App\UnitQuantity;
use App\User;
use PDF;

/*
 * Status convention
 * 0 = Masuk
 * 1 = Disetujui Kadiv
*  2 = Disetujui KaSLA
 * 3 = Diproses
 * 4 = Selesai
 */

class RequestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $whoami = whoami();
        View::share('whoami', $whoami);

        $operator = operator();
        View::share('operator', $operator);

        $manager = manager();
        View::share('manager', $manager);

        $count_requests = count_requests();
        View::share('count_requests', $count_requests);
    }
    
    public function index()
    {
        if (operator() || manager() && (session()->get('username') == 'baharipri' || session()->get('username') == 'marta')) {
            $requests = InventoryRequest::where('isShown', 1)->orderBy('created_at', 'desc')->get();            
            
            return view('inventory-request.index', compact('requests'));
        } else {
            session()->flash('flash_message', 'Anda tidak memiliki akses untuk halaman ini.');
            return redirect()->route('request-index-user', session()->get('username'));
        }
    }

    public function user($user)
    {
        if ($user == session()->get('username')) {
            if (manager() && (session()->get('username') == 'baharipri' || session()->get('username') == 'marta')) {
                $requests = InventoryRequest::where('isShown', 1)->orderBy('status', 'asc')->get();
            } elseif (manager()) {
                $requests = InventoryRequest::where('isShown', 1)
                            ->where(function($q) use($user) {
                                  $q->where('approver', $user)
                                    ->orWhere('author', $user);
                            })
                            ->orderBy('status', 'asc')
                            ->get();
            } else {
                $requests = InventoryRequest::where('isShown', 1)->where('author', $user)->orderBy('status', 'asc')->get();
            }
            return view('inventory-request.index', compact('requests'));
        } else {
            session()->flash('flash_message', 'Anda tidak memiliki akses untuk halaman tersebut.');
            return redirect()->route('request-index-user', session()->get('username'));
        }
    }

    public function detail($id)
    {
        $request = InventoryRequest::with('u_sender', 'u_receiver')->find($id);
        $inventories = RequestedInventory::where('request_id', $id)->get();

    	return view('inventory-request.detail', compact('request', 'inventories'));
    }

    public function create()
    {
        $inventories = Inventory::where('isShown', 1)
                        ->orderBy('name', 'asc')
                        ->with(['cards' => function($q) {
                            $q->orderBy('updated_at', 'desc');
                        }])
                        ->get();
        $managers = User::where('isManager', 1)
                        ->where('username', '!=', session()->get('username'))
                        ->orderBy('name')->get();
        $units = UnitQuantity::all();

        return view('inventory-request.create', compact('inventories', 'managers', 'units'));
    }

    public function store(Request $request)
    {
        $input = $request->all();
        if (validation_request($input)) {
            session()->flash('flash_message', 'Ada kesalahan pada input barang.');
            return redirect()->route('request-create')
                ->withInput();
        }
        $inventories_name = $input['inventory_name'];
        $inventories_quantity = $input['quantity'];
        $inventories_unit = $input['unit'];

        $input['author'] = session()->get('username'); 
        $input['division'] = 1;
        $new_request = new InventoryRequest;
        $new_request->author = $input['author'];
        $new_request->status = '1';
        $new_request->purpose = $input['purpose'];
        $new_request->approver = $input['approver'];
        $new_request->ticket = str_random(8);
        $new_request->isShown = 1;
        $new_request->save();

        $new_new_request = InventoryRequest::get()->last();
        
        $c = collect([collect($inventories_name)->count(), collect($inventories_quantity)->count(), collect($inventories_unit)->count()])->min();

        // iterate according to max possibility of errors
        for ($i = 0; $i < $c; $i++) {
            $new_new_request->inventories()->create([
                'inventory_id' => $inventories_name[$i],
                'quantity' => $inventories_quantity[$i],
                'unit' => $inventories_unit[$i],
            ]);
        }

        session()->flash('flash_message', 'Permintaan berhasil disimpan.');
        // if is not admin, redirect to their request
        return redirect()->route('request-detail', $new_new_request->id);
    }

    public function approveByDiv($id)
    {
        $a_request = InventoryRequest::find($id);
        if (manager() && session()->get('username') == $a_request->approver) {
            if (check_request_status($a_request, '1')) {
                InventoryRequest::where('id', $id)->update([
                    'status' => 2,
                    'approvedByDiv' => 1,
                ]);

                new_binRequest($a_request);

                session()->flash('flash_message', 'Permintaan ini telah disetujui.');
                return redirect()->route('request-detail', $id);
            } else {
                get_textStatus($a_request);
            }
        } else {
            session()->flash('flash_message', 'Anda tidak memiliki akses untuk fungsi tersebut.');
            return redirect()->route('request-detail', $id);
        }
    }

    public function rejectByDiv(Request $request, $id)
    {
        $a_request = InventoryRequest::find($id);
        if (manager() && session()->get('username') == $a_request->approver) {
            if (check_request_status($a_request, '1')) {
                $input = $request->all();
                $validator = Validator::make($request->all(), [
                    'admin_note' => 'required',
                ]);
                if ($validator->fails()) {
                    session()->flash('flash_message', 'Alasan penolakan harus diisi.');
                    return redirect()->route('request-detail')
                        ->withErrors($validator)
                        ->withInput();
                }

                InventoryRequest::where('id', $id)->update([
                    'status' => 0,
                    'admin_note' => $input['admin_note'],
                ]);

                new_binRequest($a_request);

                session()->flash('flash_message', 'Permintaan ini telah ditolak.');
                return redirect()->route('request-detail', $id);
            } else {
                get_textStatus($a_request);
            }
        } else {
            session()->flash('flash_message', 'Anda tidak memiliki akses untuk fungsi tersebut.');
            return redirect()->route('request-detail', $id);
        }
    }

    public function approveBySLA($id)
    {
        $a_request = InventoryRequest::find($id);
        if (manager() && (session()->get('username') ==  'marta' || session()->get('username') == 'baharipri')) {
            if (check_request_status($a_request, '2')) {
                InventoryRequest::where('id', $id)->update([
                    'status' => 3,
                    'approver' => session()->get('username'),
                ]);

                new_binRequest($a_request);

                session()->flash('flash_message', 'Permintaan ini telah disetujui.');
                return redirect()->route('request-detail', $id);
            } else {
                get_textStatus($a_request);
            }
        } else {
            session()->flash('flash_message', 'Anda tidak memiliki akses untuk fungsi tersebut.');
            return redirect()->route('request-detail', $id);
        }
    }

    public function rejectBySLA(Request $request, $id)
    {
        $a_request = InventoryRequest::find($id);
        if (manager() && (session()->get('username') ==  'marta' || session()->get('username') == 'baharipri')) {
            if (check_request_status($a_request, '2')) {
                $input = $request->all();
                $validator = Validator::make($request->all(), [
                    'admin_note' => 'required',
                ]);

                if ($validator->fails()) {
                    session()->flash('flash_message', 'Alasan penolakan harus diisi.');
                    return redirect()->route('request-detail', $id)
                        ->withErrors($validator)
                        ->withInput();
                }

                InventoryRequest::where('id', $id)->update([
                    'status' => 0,
                    'admin_note' => $input['admin_note'],
                ]);

                new_binRequest($a_request);

                session()->flash('flash_message', 'Permintaan ini telah ditolak.');
                return redirect()->route('request-detail', $id);
            } else {
                get_textStatus($a_request);
            }
        } else {
            session()->flash('flash_message', 'Anda tidak memiliki akses untuk fungsi tersebut.');
            return redirect()->route('request-detail', $id);
        }
    }

    public function process($id)
    {
        if (operator()) {
            $a_request = InventoryRequest::find($id);

            if (check_request_status($a_request, '3')) {
                InventoryRequest::where('id', $id)->update([
                    'status' => 4,
                    'receiver' => session()->get('username'),
                ]);

                new_binRequest($a_request);

                session()->flash('flash_message', 'Permintaan ini sedang diproses.');
                return redirect()->route('request-detail', $id);
            } else {
                get_textStatus($a_request);
            }
        } else {
            session()->flash('flash_message', 'Anda tidak memiliki akses untuk fungsi tersebut.');
            return redirect()->route('request-detail', $id);
        }
    }

    public function update($id)
    {
        if (operator()) {
            $a_request = InventoryRequest::find($id);
            $inventories = $a_request->inventories()->get();
            $units = UnitQuantity::all();
            
            return view('inventory-request.update', compact('a_request', 'inventories', 'units'));
        } else {
            session()->flash('flash_message', 'Anda tidak memiliki akses untuk halaman tersebut.');
            return redirect()->route('request-index-user', session()->get('username'));
        }
    }

    public function storeUpdate(Request $request, $id)
    {
        $input = $request->all();
        if (validation_request($input)) {
            session()->flash('flash_message', 'Ada kesalahan pada input barang.');
            return redirect()->route('request-update')
                    ->withInput();
        } else {
            $inventories_quantity = $input['quantity'];
            $inventories_unit = $input['unit'];

            $a_request = InventoryRequest::find($id);

            $r_inventories = $a_request->inventories()->get();
            $ids = [];
            $ii = 0;
            foreach ($r_inventories as $r_inventory) {
                $ids[$ii] = $r_inventory->inventory_id;
                $ii++;
            }
            // update requested inventory
            for ($i = 0; $i < count($r_inventories); $i++) {
                RequestedInventory::where('request_id', $id)->where('inventory_id', $ids[$i])
                    ->update([
                        'quantity' => $inventories_quantity[$i],
                        'unit' => $inventories_unit[$i],
                    ]);
            }
            new_binRequestedInventory($a_request);
            // update this request
            if ($input['admin_note'] != '') {
                InventoryRequest::where('id', $id)->update([
                    'admin_note' => $input['admin_note'],
                    'status' => 2,
                ]);
                new_binRequest($a_request);
            }

            session()->flash('flash_message', 'Permintaan berhasil diubah.');
            return redirect()->route('request-detail', $id);
        }
    }


    public function finish(Request $request, $id)
    {
        if (operator()) {
            $input = $request->all();
            $a_request = InventoryRequest::find($id);
            if (check_request_status($a_request, '4')) {
                if($input['ticket'] == $a_request->ticket) {
                    InventoryRequest::where('id', $id)->update([
                        'status' => 5,
                        'receiver' => session()->get('username'),
                    ]);

                    $r_inventories = $a_request->inventories()->get();
                    new_cards($r_inventories, $a_request);
                    
                    new_binRequest($a_request);

                    session()->flash('flash_message', 'Permintaan ini telah selesai diproses.');
                    return redirect()->route('request-detail', $id); 
                }
                else {
                    session()->flash('flash_message', 'Kode pengambilan yang dimasukkan salah.');
                    return redirect()->route('request-detail', $id); 
                }
            } else {
                get_textStatus($a_request);
            }
        } else {
            session()->flash('flash_message', 'Anda tidak memiliki akses untuk fungsi tersebut.');
            return redirect()->route('request-detail', $id);
        }
    }

    public function delete(Request $request, $id)
    {
        if (operator()) {
            $a_request = InventoryRequest::find($id);

            InventoryRequest::where('id', $id)->update([
                    'isShown' => 0,
                ]);

            new_binRequest($a_request);

            session()->flash('flash_message', 'Permintaan berhasil dihapus.');
            return redirect()->route('request-index');
        } else {
            session()->flash('flash_message', 'Anda tidak memiliki akses untuk halaman ini.');
            return redirect()->route('request-index-user', session()->get('username'));
        }
    }

    public function export($id)
    {
        $request = InventoryRequest::with('u_sender', 'u_receiver')->find($id);
        $inventories = RequestedInventory::where('request_id', $id)->get();
        
        $pdf = PDF::loadView('inventory-request.print', compact('request', 'inventories'));
        return $pdf->download('Bon Permintaan #'.$request->id.'.pdf');
    }
}
