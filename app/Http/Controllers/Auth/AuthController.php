<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    // protected $redirectTo = '/';
    // protected $loginView = 'auth.login';

    public function loginForm()
    {
        if (operator()) {
            return redirect()->route('index');
        }

        return view('auth.login');
    }

    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('login')
                ->withErrors($validator)
                ->withInput();
        }

        $input = $request->all();
        $username = $input['username'];
        $password = $input['password'];
        // return $input;
        $adServer = 'corp.bi.go.id';
        $ldapConn = ldap_connect($adServer);
        $ldaprdn = 'bank_indonesia' . '\\' . $username;
        // return dd($ldapConn);
        ldap_set_option($ldapConn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ldapConn, LDAP_OPT_REFERRALS, 0);

        $bind = @ldap_bind($ldapConn, $ldaprdn, $password);
        // return dd($bind);

        if ($bind) {
            // return "cek1";
            $filter = "(sAMAccountName=$username)"; // type exactly like this to parse the username
            // echo var_dump($filter);
            $result = ldap_search($ldapConn, 'dc=corp,dc=bi,dc=go,dc=id', $filter);
            // echo var_dump($result);
            ldap_sort($ldapConn, $result, 'sn');
            $info = ldap_get_entries($ldapConn, $result);
            // return dd($info);
            for ($i = 0; $i < $info['count']; $i++) {
                if($info['count'] > 1)
                    break;

                // store session
                session([
                    'username' => $info[$i]['samaccountname'][0],
                    'name' => $info[$i]['givenname'][0] .' '. $info[$i]['sn'][0],
                ]);


                // return User::where('nip', session()->get('nip'))->exists();

                // create user if not exist on user table
                if (!User::where('username', session()->get('username'))->exists()) {
                    $user = new User;
                    $user->username = session()->get('username');
                    $user->nip = $info[$i]['employeeid'][0];
                    $user->division = $info[$i]['department'][0];
                    $user->email = $info[$i]['mail'][0];
                    $user->name = session()->get('name');
                    $user->isShown = 1;
                    $user->save();
                }
                if (User::where('username', session()->get('username'))->exists() && User::find(session()->get('username'))->isShown == false) {
                    $user = User::find(session()->get('username'));

                    User::where('username', session()->get('username'))->update([
                        'isShown' => 1,
                    ]);

                    new_binUser($user);
                }
                // return dd(User::find(session()->get('username'))->isShown);
            }
            @ldap_close($ldapConn);
            return redirect()->route('index');
        } else {
            // return 'why';
            session()->flash('flash_message', 'Terjadi kesalahan input.');
            return view('auth.login');
        }
    }

    public function logout()
    {
        session()->forget('username');
        session()->forget('nip');
        session()->forget('division');
        session()->forget('name');

        session()->flush();
        return redirect()->route('login');
    }
}