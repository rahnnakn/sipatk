# Development Configuration
1. Rename `.env.sample` into `.env` and set the DB settings in it according to your own environment configuration
2. Run `php artisan key:generate`
3. Run `php artisan migrate:refresh` to migrate the data tables
4. Run `php artisan db:seed` to migrate the master data
5. Run `composer install`
6. Run `npm install`
7. Run `gulp`
8. Run `gulp watch` if planning to make any change on frontend
9. Run `php artisan serve`
10. Hack away!

# Code Wiki
## Models
Each model represent a table and its relationship with other tables.
## Controllers
Most logic are presented on each respective modul. InventoryController for inventory, and so on.
## Helpers
If certain function is to be executed by more than one controller, then putting the it in the `helpers.php` file is a good way to increase efficiency. It can be considered as an act of making sure your code is maintainable and therefore can be reused regardless which modul calling it. These helpers are: 
1. Returning the currently logged in person information
2. Check if the currently logged in person is an operator
3. Check if the currently logged in person is a manager
4. Count new requests for each respective role
5. Get Current Stock
6. Create New Bin for Inventory Category
7. Create New Bin for Inventory
8. Create New Bin for User
9. Create New Bin for Request
10. Create New Bin for Requested Inventory
11. Create New Cards for Finished Request
12. Create A Card for New Inventory
13. Set Inventory Status on Update
14, Do a Validation for A Request
15. Get a respond text for certain status of a request
16. Check if the needed status of a request is true
17. Get the amount of coming for this inventory per month
18. Get the amount of leaving for this inventory per month
19. Get the total of coming for this inventory per year
20. Get the total of leaving for this inventory per year