<head>
    <style>
        td, th {
            border: 1px solid black;
        }
        td.no-top {
            border-top-style: hidden;
        }
        td.no-bottom {
            border-bottom-style: hidden;
        }
        td.no-left {
            border-left-style: hidden;
        }
        td.no-right {
            border-right-style: hidden;
        }
        td.no-border {
            border-style: hidden;
        }
    </style>
</head>
<img src="assets/images/logobi.png">
<h2 style="text-align:center">Kartu Persediaan Alat-Alat Keperluan Kantor</h2>
<table style="width:100%;text-align:center;border-collapse:collapse">
    <tr>
        <td rowspan="2" colspan="2">Catatan pemesanan</td>
        <td class="border-right">Kontrol</td>
        <td class="no-top no-right no-bottom"></td>
        <td class="no-border"></td>
        <td class="no-top no-left"></td>
        <td colspan="2">Nomor Form BI</td>
    </tr>
    <tr>
        <td>...</td>
        <td class="no-top"></td>
        <td class="no-border"></td>
        <td class="no-top no-left"></td>
        <td colspan="2">...</td>
    </tr>
    <tr>
        <td rowspan="4" colspan="2">...<br>...<br>...<br>...</td>
        <td colspan="2" class="no-bottom">Pemakaian</td>
        <td class="border-right no-bottom">{{ $transactions->count() }}</td>
        <td colspan="3">Nama ATK</td>
    </tr>
    <tr>
        <td colspan="2" class="no-top">tahun {{ $year }}</td>
        <td class="no-top">transaksi</td>
        <td colspan="3" rowspan="3">{{ $inventory->name }}</td>
    </tr>
    <tr>
        <td colspan="2" class="no-bottom">Persediaan minimum</td>
        <td class="no-bottom">{{ $inventory->min_stock }}</td>
    </tr>
    <tr>
        <td colspan="2" class="no-top">(pemakaian 6 bln.)</td>
        <td class="no-top">{{ $inventory->unit }}</td>
    </tr>
    <tr>
        <th>Waktu Dibuat</th>
        <th>No. Bon</th>
        <th>Tanggal Transaksi</th>
        <th>Dari</th>
        <th>Kepada</th>
        <th>Masuk</th>
        <th>Keluar</th>
        <th>Stok</th>
    </tr>
    @foreach ($transactions as $transaction)
        <tr>
            <td>{{ date_format($transaction->created_at, 'd-m-Y') }}</td>
            <td>
                @if ($transaction->bill_num == "")
                    -
                @else
                    {{ $transaction->bill_num }}
                @endif
            </td>
            <td>{{ date_format (new DateTime($transaction->transaction_date), 'd-m-Y') }}</td>
            <td>
                @if ($transaction->form == "")
                    -
                @else
                    {{ $transaction->form }}
                @endif
            </td>
            <td>
                @if ($transaction->to == "")
                    -
                @else
                    {{ $transaction->to }}
                @endif
            </td>
            <td>{{ $transaction->in }}</td>
            <td>{{ $transaction->out }}</td>
            <td>{{ $transaction->stock }}</td>
        </tr>
    @endforeach
    {{--<tbody>--}}
    {{--@foreach ($card as $a_card)--}}
        {{--<tr>--}}
            {{--<td>{{ $a_card->created_at }}</td>--}}
            {{--<td>{{ $a_card->bill_num }}</td>--}}
            {{--<td>{{ $a_card->transaction_date }}</td>--}}
            {{--<td>{{ $a_card->from ?: "-"}}</td>--}}
            {{--<td>{{ $a_card->to ?: "-"}}</td>--}}
            {{--<td>{{ $a_card->in }}</td>--}}
            {{--<td>{{ $a_card->out }}</td>--}}
            {{--<td>{{ $a_card->stock }}</td>--}}
        {{--</tr>--}}
    {{--@endforeach--}}
    {{--</tbody>--}}
</table>