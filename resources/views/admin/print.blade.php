<div class="container col s12 m8 l9">
    <h4 class="header">Daftar Admin</h4>
    <div class="row">
        <div class="col s12 m12 l12">
            <table class="display centered" cellspacing="0">
                <thead>
                <tr>
                    <th>NIP</th>
                    <th>Nama</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Divisi</th>
                    <th>Role</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->nip }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->division }}</td>
                        <td>
                            @if ($user->isOperator && $user->isManager)
                                Admin
                            @elseif ($user->isOperator)
                                Operator
                            @elseif ($user->isManager)
                                Manager
                            @else
                                Pengguna
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>