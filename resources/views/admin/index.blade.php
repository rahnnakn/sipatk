@extends('layouts.admin')
@section('page-title', 'Daftar Admin | ')
@section('title', 'Daftar Admin')
@section('root')
    <li><a class="blue-text text-darken-4" href="{{ route('index') }}">Dashboard</a></li>
@endsection
@section('here', 'Daftar Admin')
@if ($operator || $manager)
  @section('new_request', $count_requests)
@endif
@section('styles')
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.6/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('assets/js/plugins/data-tables/css/jquery.dataTables.min.css') }}">
@endsection
@section('admin-content')
    <div class="container col s12 m8 l9">
        <div class="right">
            <a href="{{ route('admin-create') }}" class="btn waves-effect waves-light indigo darken-4"><i class="mdi-content-add"></i></a>
        </div>
        <div id="table-datatables">
            <h4 class="header">Daftar Admin</h4>
            <div class="row">
                <div class="col s12 m12 l12">
                    @if (session()->has('flash_message'))
                        <div id="card-alert" class="card blue darken-1">
                            <div class="card-content white-text darken-1">
                                <p class="single-alert">{{ session('flash_message') }}</p>
                            </div>
                        </div>
                        <br>
                    @endif
                    <table id="data-table-simple" class="responsive-table display centered" cellspacing="0">
                        <thead>
                        <tr>
                            <th>NIP</th>
                            <th>Nama</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Divisi</th>
                            <th>Peran</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>NIP</th>
                            <th>Nama</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Divisi</th>
                            <th>Peran</th>
                            <th></th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->nip }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->username }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->division }}</td>
                                <td>
                                    @if ($user->isOperator && $user->isManager)
                                        Admin
                                    @elseif ($user->isOperator)
                                        Operator
                                    @elseif ($user->isManager)
                                        Manager
                                    @else
                                        Pengguna
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('admin-update', $user->username) }}" class="btn waves-effect waves-light blue"><i class="mdi-editor-border-color"></i></a>
                                    <a href="{{ route('admin-delete', $user->username) }}" class="btn waves-effect waves-light light-blue darken-2 delete-admin"><i class="mdi-content-clear"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/js/plugins/data-tables/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/data-tables/data-tables-script.js') }}"></script>
@endsection