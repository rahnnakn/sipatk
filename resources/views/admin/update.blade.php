@extends('layouts.admin')
@section('page-title', 'Ubah Admin | ')
@section('title', 'Ubah Admin')
@section('root')
    <li><a class="blue-text text-darken-4" href="{{ route('index') }}">Dashboard</a></li>
@endsection
@section('here', 'Ubah Admin')
@if ($operator || $manager)
  @section('new_request', $count_requests)
@endif
@section('styles')
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.6/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('assets/js/plugins/data-tables/css/jquery.dataTables.min.css') }}">
@endsection
@section('admin-content')
    <div class="col s12 m8 l9">
        <div id="jqueryvalidation" class="section">
            <div class="container">
                <h3 class="header">Ubah Admin</h3>
                <div id="card-alert" class="card blue darken-1">
                    <div class="card-content white-text">
                        <p>Area dengan (*) wajib diisi.</p>
                        @if (session()->has('flash_message'))
                            <p class="single-alert">{{ session('flash_message') }}</p>
                        @endif
                    </div>
                </div>
                <br>
                <form class="row formValidate" id="formValidate" novalidate="novalidate" method="post" action="{{ route('admin-update', $user->username) }}">
                    {{ csrf_field() }}
                    <div class="col s12 m6 l6 quantity validate">
                        <label for="username">Username*</label>
                        <input type="text" name="username" value="{{ $user->username }}" data-error=".errorUsername">
                        <div class="errorUsername"></div>
                    </div>
                    <div class="col s12 m6 l6 quantity validate">
                        <label for="email">Email*</label>
                        <input type="email" name="email" value="{{ $user->email }}" data-error=".errorEmail">
                        <div class="errorEmail"></div>
                    </div>
                    <div class="col s12 m8 l8 quantity validate">
                        <label for="name">Nama*</label>
                        <input type="text" name="name" value="{{ $user->name }}" data-error=".errorName">
                        <div class="errorName"></div>
                    </div>
                    <div class="col s12 m4 l4 quantity validate">
                        <label for="nip">NIP*</label>
                        <input type="text" name="nip" value="{{ $user->nip }}" data-error=".errorNip">
                        <div class="errorNip"></div>
                    </div>
                    <div class="col s12 m8 l8 quantity validate">
                        <label for="division">Divisi</label>
                        <input type="text" name="division" value="{{ $user->division }}" data-error=".errorDivisi">
                        <div class="errorDivisi"></div>
                    </div>
                    <div class="col s12 m4 l4 validate">
                        <label for="role">Role</label>
                        <select name="role" data-error=".errorAdmin" required="required">
                            @if ($user->isOperator)
                                <option value="1" selected>Operator</option>
                                <option value="2">Manajer</option>
                                <option value="0">Pengguna</option>
                            @elseif ($user->isManager)
                                <option value="2" selected>Manajer</option>
                                <option value="0">Pengguna</option>
                                <option value="1">Operator</option>
                            @else
                                <option value="0" selected>Pengguna</option>
                                <option value="1">Operator</option>
                                <option value="2">Manajer</option>
                            @endif
                        </select>
                        <div class="errorAdmin"></div>
                    </div>
                    <div class="col s12 m12 l12">
                        <button class="btn waves-effect waves-light indigo darken-4 update-admin">Simpan</button>
                    </div>
                </form>
                <!-- </div> -->
                <!-- </div> -->
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('assets/js/jquery-validation.min.js') }}"></script>
    <script>
        $("#formValidate").validate({
            rules: {
                username: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                name: {
                    required: true,
                },
                nip: {
                    required: true,
                },
                role: {
                    required: true,
                }
            },
            //For custom messages
            messages: {
                username: {
                    required: "Username harus diisi.",
                },
                email: {
                    required: "Email harus diisi.",
                    email: "Format email salah."
                },
                name: {
                    required: "Nama harus diisi.",
                },
                nip: {
                    required: "NIP harus diisi.",
                },
                role: {
                    required: "Role harus diisi.",
                }
            },
            errorElement : 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error);
                } else {
                    error.insertAfter(element);
                }
            }
        });
    </script>
@endsection