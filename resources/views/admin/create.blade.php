@extends('layouts.admin')
@section('page-title', 'Tambah Admin | ')
@section('title', 'Tambah Admin')
@section('root')
    <li><a class="blue-text text-darken-4" href="{{ route('index') }}">Dashboard</a></li>
@endsection
@section('here', 'Tambah Admin')
@if ($operator || $manager)
  @section('new_request', $count_requests)
@endif
@section('styles')
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.6/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('assets/js/plugins/data-tables/css/jquery.dataTables.min.css') }}">
@endsection
@section('admin-content')
    <div class="col s12 m8 l9">
        <div id="jqueryvalidation" class="section">
            <div class="container">
                <h3 class="header">Tambah Admin</h3>
                <div id="card-alert" class="card blue darken-1">
                    <div class="card-content white-text">
                        <p>Area dengan (*) wajib diisi.</p>
                        @if (session()->has('flash_message'))
                            <p class="single-alert">{{ session('flash_message') }}</p>
                        @endif
                    </div>
                </div>
                <br>
                <form class="row formValidate" id="formValidate" novalidate="novalidate" method="post" action="{{ route('admin-create') }}">
                    {{ csrf_field() }}
                    <div class="input-field col s12 m6 l6 quantity validate">
                        <label for="username">Username*</label>
                        <input type="text" name="username" data-error=".errorUsername">
                        <div class="errorUsername"></div>
                    </div>
                    <div class="input-field col s12 m6 l6 quantity validate">
                        <label for="email">Email*</label>
                        <input type="email" name="email" data-error=".errorEmail">
                        <div class="errorEmail"></div>
                    </div>
                    <div class="input-field col s12 m8 l8 quantity validate">
                        <label for="name">Nama*</label>
                        <input type="text" name="name" data-error=".errorName">
                        <div class="errorName"></div>
                    </div>
                    <div class="input-field col s12 m4 l4 quantity validate">
                        <label for="nip">NIP*</label>
                        <input type="text" name="nip" data-error=".errorNip">
                        <div class="errorNip"></div>
                    </div>
                    <div class="input-field col s12 m8 l8 quantity validate">
                        <label for="division">Divisi</label>
                        <input type="text" name="division" data-error=".errorDivisi">
                        <div class="errorDivisi"></div>
                    </div>
                    <div class="input-field col s12 m4 l4 validate">
                        <select name="role" data-error=".errorAdmin" required="required">
                            <option value="" disabled selected>Peran*</option>
                            <option value="0">Pengguna</option>
                            <option value="1">Operator</option>
                            <option value="2">Manajer</option>
                        </select>
                        <div class="errorAdmin"></div>
                    </div>
                    <div class="input-field col s12 m12 l12">
                        <button class="btn waves-effect waves-light indigo darken-4 create-admin">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('assets/js/jquery-validation.min.js') }}"></script>
    <script>
        $("#formValidate").validate({
            rules: {
                username: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                name: {
                    required: true,
                },
                nip: {
                    required: true,
                },
                role: {
                    required: true,
                }
            },
            //For custom messages
            messages: {
                username: {
                    required: "Username harus diisi.",
                },
                email: {
                    required: "Email harus diisi.",
                    email: "Format email salah.",
                },
                name: {
                    required: "Nama harus diisi.",
                },
                nip: {
                    required: "NIP harus diisi.",
                },
                role: {
                    required: "Role harus diisi.",
                }
            },
            errorElement : 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error);
                } else {
                    error.insertAfter(element);
                }
            }
        });
    </script>
@endsection