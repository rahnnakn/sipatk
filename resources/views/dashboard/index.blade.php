@extends('layouts.admin')
@section('page-title', 'Dashboard | ')
@section('title', 'Dashboard')
@section('here', 'Dashboard')
@section('new_request', $count_requests)
@section('styles')
<link rel="stylesheet" href="{{ asset('assets/js/plugins/chartist-js/chartist.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
<link rel="stylesheet" href="http://cdn.datatables.net/1.10.6/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('assets/js/plugins/data-tables/css/jquery.dataTables.min.css') }}">
@endsection
@section('admin-content')
<br>
@if ($operator || $manager && (session('username') == 'baharipri' || session('username') == 'marta')) 
    <div class="container">
        <a href="{{ route('dashboard-export') }}" class="btn waves-effect waves-light blue tooltipped" data-position="bottom" data-delay="1" data-tooltip="Cetak Dashboard" style="margin-bottom:20px;margin-top:15px"><i class="mdi-action-print"></i></a>
    </div>
@endif
    <div class="container">
        <div class="row">
            <div class="col s12 m6 l6">
                <h5 class="red-text text-darken-4" style="text-align:center;">Permintaan ATK Terbanyak</h5>
                <table class="centered striped responsive-table">
                    <thead class="red darken-4">
                      <tr>
                        <th data-field="rank" style="color:white;">No.</th>
                        <th data-field="name" style="color:white;">Nama ATK</th>
                        <th data-field="price" style="color:white;">Jumlah Permintaan</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php $c = 1?>
                        @foreach ($inventories as $inventory)
                        <tr>
                            <td>{{ $c++ }}</td>
                            <td>{{ $inventory->name }}</td>
                            <td>{{ $inventory->total }} {{ $inventory->unit }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="col s12 m6 l6">
                <h5 class="indigo-text text-darken-4" style="text-align:center;">Divisi dengan Permintaan Terbanyak</h5>
                <table class="centered striped responsive-table">
                    <thead class="indigo darken-4">
                      <tr>
                        <th data-field="rank" style="color:white;">No.</th>
                        <th data-field="name" style="color:white;">Divisi</th>
                        <th data-field="price" style="color:white;">Jumlah Permintaan</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $c = 1?>
                        @foreach ($requests as $request)
                        <tr>
                            <td>{{ $c++ }}</td>
                            <td>{{ $request->division }}</td>
                            <td>{{ $request->total }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <br>
        @if ($operator || $manager && (session('username') == 'baharipri' || session('username') == 'marta')) 
        <div class="row">
            <div class="col s12 m6 l6">
                <!-- <div id="paper" class="right">
                    <select>
                        @foreach ($papers as $paper)
                            <option>{{ $paper->year }}</option>
                        @endforeach
                    </select>
                </div> -->
                <h5 class="indigo-text text-darken-4" style="text-align:center;">Permintaan Kertas</h5>
                <table class="centered striped responsive-table">
                    <thead class="indigo darken-4">
                      <tr>
                        <th data-field="rank" style="color:white;">No.</th>
                        <th data-field="name" style="color:white;">Bulan</th>
                        <th data-field="name" style="color:white;">Nama ATK</th>
                        <th data-field="price" style="color:white;">Jumlah Permintaan</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $c = 1?>
                        @foreach ($papers as $paper)
                          <tr>
                            <td>{{ $c++ }}</td>
                            <td>{{ $paper->month }} {{ $paper->year }}</td>
                            <td>{{ $paper->name }}</td>
                            <td>{{ $paper->total }} {{ $paper->unit }}</td>
                          </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="col s12 m6 l6">
                <!-- <div id="toner" class="right">
                    <select>
                        @foreach ($toners as $toner)
                            <option>{{ $toner->year }}</option>
                        @endforeach
                    </select>
                </div> -->
                <h5 class="indigo-text text-darken-4" style="text-align:center;">Permintaan Toner</h5>
                <table class="centered striped responsive-table">
                    <thead class="red darken-4">
                      <tr>
                        <th data-field="rank" style="color:white;">No.</th>
                        <th data-field="name" style="color:white;">Bulan</th>
                        <th data-field="name" style="color:white;">Nama ATK</th>
                        <th data-field="price" style="color:white;">Jumlah Permintaan</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $c = 1?>
                        @foreach ($toners as $toner)
                        <tr>
                            <td>{{ $c++ }}</td>
                            <td>{{ $toner->month }} {{ $toner->year }}</td>
                            <td>{{ $toner->name }}</td>
                            <td>{{ $toner->total }} {{ $toner->unit }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endif
    </div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('assets/js/plugins/data-tables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/data-tables/data-tables-script.js') }}"></script>
@endsection