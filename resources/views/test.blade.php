<html lang="en"><!--================================================================================
    Item Name: Materialize - Material Design Admin Template
    Version: 3.1
    Author: GeeksLabs
    Author URL: http://www.themeforest.net/user/geekslabs
================================================================================ --><head><style type="text/css">@charset "UTF-8";[ng\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide:not(.ng-hide-animate){display:none !important;}ng\:form{display:block;}.ng-animate-shim{visibility:hidden;}.ng-anchor{position:absolute;}</style>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Forms Validation | Materialize - Material Design Admin Template</title>

  <!-- Favicons-->
  <link rel="icon" href="images/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
  <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="css/style.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <!-- Custome CSS-->    
  <link href="css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">

  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <style type="text/css">
  .input-field div.error{
    position: relative;
    top: -1rem;
    left: 0rem;
    font-size: 0.8rem;
    color:#FF4081;
    -webkit-transform: translateY(0%);
    -ms-transform: translateY(0%);
    -o-transform: translateY(0%);
    transform: translateY(0%);
  }
  .input-field label.active{
      width:100%;
  }
  .left-alert input[type=text] + label:after, 
  .left-alert input[type=password] + label:after, 
  .left-alert input[type=email] + label:after, 
  .left-alert input[type=url] + label:after, 
  .left-alert input[type=time] + label:after,
  .left-alert input[type=date] + label:after, 
  .left-alert input[type=datetime-local] + label:after, 
  .left-alert input[type=tel] + label:after, 
  .left-alert input[type=number] + label:after, 
  .left-alert input[type=search] + label:after, 
  .left-alert textarea.materialize-textarea + label:after{
      left:0px;
  }
  .right-alert input[type=text] + label:after, 
  .right-alert input[type=password] + label:after, 
  .right-alert input[type=email] + label:after, 
  .right-alert input[type=url] + label:after, 
  .right-alert input[type=time] + label:after,
  .right-alert input[type=date] + label:after, 
  .right-alert input[type=datetime-local] + label:after, 
  .right-alert input[type=tel] + label:after, 
  .right-alert input[type=number] + label:after, 
  .right-alert input[type=search] + label:after, 
  .right-alert textarea.materialize-textarea + label:after{
      right:70px;
  }
  </style>
<style type="text/css"></style></head>

<body class="loaded">
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START HEADER -->
  <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="index.html" class="brand-logo darken-1"><img src="images/materialize-logo.png" alt="materialize logo"></a> <span class="logo-text">Materialize</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi-action-search"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
                    </div>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button" data-activates="translation-dropdown"><img src="images/flag-icons/United-States.png" alt="USA"></a><ul id="translation-dropdown" class="dropdown-content">
                      <li>
                        <a href="#!"><img src="images/flag-icons/United-States.png" alt="English">  <span class="language-select">English</span></a>
                      </li>
                      <li>
                        <a href="#!"><img src="images/flag-icons/France.png" alt="French">  <span class="language-select">French</span></a>
                      </li>
                      <li>
                        <a href="#!"><img src="images/flag-icons/China.png" alt="Chinese">  <span class="language-select">Chinese</span></a>
                      </li>
                      <li>
                        <a href="#!"><img src="images/flag-icons/Germany.png" alt="German">  <span class="language-select">German</span></a>
                      </li>
                      
                    </ul>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i>
                        
                        </a><ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; position: absolute; top: 64px; left: 1055px; opacity: 1; display: none;">
                      <li>
                        <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                      </li>
                    </ul>
                        </li>                        
                        <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
                        </li>
                    </ul>
                    <!-- translation-button -->
                    
                    <!-- notifications-dropdown -->
                    
                </div>
            </nav>
        </div>
        <!-- end header nav-->
  </header>
  <!-- END HEADER -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START MAIN -->
  <div id="main">
    <!-- START WRAPPER -->
    <div class="wrapper">

      <!-- START LEFT SIDEBAR NAV-->
      <aside id="left-sidebar-nav">
        <ul id="slide-out" class="side-nav fixed leftside-navigation ps-container ps-active-y" style="width: 240px; left: -250px;">
            <li class="user-details cyan darken-2">
            <div class="row">
                <div class="col col s4 m4 l4">
                    <img src="images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                </div>
                <div class="col col s8 m8 l8">
                    
                    <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown">John Doe<i class="mdi-navigation-arrow-drop-down right"></i></a><ul id="profile-dropdown" class="dropdown-content">
                        <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                        </li>
                        <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                        </li>
                        <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a>
                        </li>
                        <li><a href="#"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                        </li>
                    </ul>
                    <p class="user-roal">Administrator</p>
                </div>
            </div>
            </li>
            <li class="bold"><a href="index.html" class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Dashboard</a>
            </li>
            <li class="no-padding">
                <ul class="collapsible collapsible-accordion">
                    <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Layouts</a>
                        <div class="collapsible-body" style="">
                            <ul>
                                <li><a href="layout-fullscreen.html">Full Screen</a>
                                </li>
                                <li><a href="layout-horizontal-menu.html">Horizontal Menu</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="bold"><a href="app-email.html" class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Mailbox <span class="new badge">4</span></a>
            </li>
            <li class="bold"><a href="app-calendar.html" class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> Calender</a>
            </li>
            <li class="no-padding">
                <ul class="collapsible collapsible-accordion">
                    <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-action-invert-colors"></i> CSS</a>
                        <div class="collapsible-body" style="">
                            <ul>
                                <li><a href="css-typography.html">Typography</a>
                                </li>
                                <li><a href="css-icons.html">Icons</a>
                                </li>
                                <li><a href="css-animations.html">Animations</a>
                                </li>
                                <li><a href="css-shadow.html">Shadow</a>
                                </li>
                                <li><a href="css-media.html">Media</a>
                                </li>
                                <li><a href="css-sass.html">Sass</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i> UI Elements</a>
                        <div class="collapsible-body" style="">
                            <ul>
                                <li><a href="ui-alerts.html">Alerts</a>
                                </li>
                                <li><a href="ui-buttons.html">Buttons</a>
                                </li>
                                <li><a href="ui-badges.html">Badges</a>
                                </li>
                                <li><a href="ui-breadcrumbs.html">Breadcrumbs</a>
                                </li>
                                <li><a href="ui-collections.html">Collections</a>
                                </li>
                                <li><a href="ui-collapsibles.html">Collapsibles</a>
                                </li>
                                <li><a href="ui-tabs.html">Tabs</a>
                                </li>
                                <li><a href="ui-navbar.html">Navbar</a>
                                </li>
                                <li><a href="ui-pagination.html">Pagination</a>
                                </li>
                                <li><a href="ui-preloader.html">Preloader</a>
                                </li>
                                <li><a href="ui-toasts.html">Toasts</a>
                                </li>
                                <li><a href="ui-tooltip.html">Tooltip</a>
                                </li>
                                <li><a href="ui-waves.html">Waves</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-av-queue"></i> Advanced UI <span class="new badge"></span></a>
                        <div class="collapsible-body" style="">
                            <ul>
                                <li><a href="advanced-ui-chips.html">Chips</a>
                                </li>
                                <li><a href="advanced-ui-cards.html">Cards</a>
                                </li>
                                <li><a href="advanced-ui-modals.html">Modals</a>
                                </li>
                                <li><a href="advanced-ui-media.html">Media</a>
                                </li>
                                <li><a href="advanced-ui-range-slider.html">Range Slider</a>
                                </li>
                                <li><a href="advanced-ui-sweetalert.html">SweetAlert</a>
                                </li>
                                <li><a href="advanced-ui-nestable.html">Shortable &amp; Nestable</a>
                                </li>
                                <li><a href="advanced-ui-translation.html">Language Translation</a>
                                </li>
                                <li><a href="advanced-ui-highlight.html">Highlight</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="bold"><a href="app-widget.html" class="waves-effect waves-cyan"><i class="mdi-device-now-widgets"></i> Widgets</a>
                    </li>
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-editor-border-all"></i> Tables</a>
                        <div class="collapsible-body" style="">
                            <ul>
                                <li><a href="table-basic.html">Basic Tables</a>
                                </li>
                                <li><a href="table-data.html">Data Tables</a>
                                </li>
                                <li><a href="table-jsgrid.html">jsGrid</a>
                                </li>
                                <li><a href="table-editable.html">Editable Table</a>
                                </li>
                                <li><a href="table-floatThead.html">floatThead</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="bold active"><a class="collapsible-header  waves-effect waves-cyan active"><i class="mdi-editor-insert-comment"></i> Forms <span class="new badge"></span></a>
                        <div class="collapsible-body" style="display: block;">
                            <ul>
                                <li><a href="form-elements.html">Form Elements</a>
                                </li>
                                <li><a href="form-layouts.html">Form Layouts</a>
                                </li>
                                <li class="active"><a href="form-validation.html">Form Validations</a>
                                </li>
                                <li><a href="form-masks.html">Form Masks</a>
                                </li>
                                <li><a href="form-file-uploads.html">File Uploads</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-social-pages"></i> Pages</a>
                        <div class="collapsible-body" style="">
                            <ul>                                        
                                <li><a href="page-contact.html">Contact Page</a>
                                </li>
                                <li><a href="page-todo.html">ToDos</a>
                                </li>
                                <li><a href="page-blog-1.html">Blog Type 1</a>
                                </li>
                                <li><a href="page-blog-2.html">Blog Type 2</a>
                                </li>
                                <li><a href="page-404.html">404</a>
                                </li>
                                <li><a href="page-500.html">500</a>
                                </li>
                                <li><a href="page-blank.html">Blank</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-action-shopping-cart"></i> eCommers</a>
                        <div class="collapsible-body" style="">
                            <ul>
                                <li><a href="eCommerce-products-page.html">Products Page</a>
                                </li>                                        
                                <li><a href="eCommerce-pricing.html">Pricing Table</a>
                                </li>
                                <li><a href="eCommerce-invoice.html">Invoice</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-image"></i> Medias</a>
                        <div class="collapsible-body" style="">
                            <ul>                                        
                                <li><a href="media-gallary-page.html">Gallery Page</a>
                                </li>
                                <li><a href="media-hover-effects.html">Image Hover Effects</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-action-account-circle"></i> User</a>
                        <div class="collapsible-body" style="">
                            <ul>     
                                <li><a href="user-profile-page.html">User Profile</a>
                                </li>                                   
                                <li><a href="user-login.html">Login</a>
                                </li>                                        
                                <li><a href="user-register.html">Register</a>
                                </li>
                                <li><a href="user-forgot-password.html">Forgot Password</a>
                                </li>
                                <li><a href="user-lock-screen.html">Lock Screen</a>
                                </li>                                        
                                
                            </ul>
                        </div>
                    </li>
                    
                    <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-editor-insert-chart"></i> Charts</a>
                        <div class="collapsible-body" style="">
                            <ul>
                                <li><a href="charts-chartjs.html">Chart JS</a>
                                </li>
                                <li><a href="charts-chartist.html">Chartist</a>
                                </li>
                                <li><a href="charts-morris.html">Morris Charts</a>
                                </li>
                                <li><a href="charts-xcharts.html">xCharts</a>
                                </li>
                                <li><a href="charts-flotcharts.html">Flot Charts</a>
                                </li>
                                <li><a href="charts-sparklines.html">Sparkline Charts</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="li-hover"><div class="divider"></div></li>
            <li class="li-hover"><p class="ultra-small margin more-text">MORE</p></li>
            <li><a href="angular-ui.html"><i class="mdi-action-verified-user"></i> Angular UI  <span class="new badge"></span></a>
            </li>
            <li><a href="css-grid.html"><i class="mdi-image-grid-on"></i> Grid</a>
            </li>
            <li><a href="css-color.html"><i class="mdi-editor-format-color-fill"></i> Color</a>
            </li>
            <li><a href="css-helpers.html"><i class="mdi-communication-live-help"></i> Helpers</a>
            </li>
            <li><a href="changelogs.html"><i class="mdi-action-swap-vert-circle"></i> Changelogs</a>
            </li>                    
            <li class="li-hover"><div class="divider"></div></li>
            <li class="li-hover"><p class="ultra-small margin more-text">Daily Sales</p></li>
            <li class="li-hover">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="sample-chart-wrapper">                            
                            <div class="ct-chart ct-golden-section" id="ct2-chart"><svg xmlns:ct="http://gionkunz.github.com/chartist-js/ct" width="100%" height="100%" class="ct-chart-line" style="width: 100%; height: 100%;"><g class="ct-labels"><foreignObject style="overflow: visible;" x="45" y="105" width="21.125" height="30"><span class="ct-label ct-horizontal" xmlns="http://www.w3.org/1999/xhtml">1</span></foreignObject><foreignObject style="overflow: visible;" x="66.125" y="105" width="21.125" height="30"><span class="ct-label ct-horizontal" xmlns="http://www.w3.org/1999/xhtml">2</span></foreignObject><foreignObject style="overflow: visible;" x="87.25" y="105" width="21.125" height="30"><span class="ct-label ct-horizontal" xmlns="http://www.w3.org/1999/xhtml">3</span></foreignObject><foreignObject style="overflow: visible;" x="108.375" y="105" width="21.125" height="30"><span class="ct-label ct-horizontal" xmlns="http://www.w3.org/1999/xhtml">4</span></foreignObject><foreignObject style="overflow: visible;" x="129.5" y="105" width="21.125" height="30"><span class="ct-label ct-horizontal" xmlns="http://www.w3.org/1999/xhtml">5</span></foreignObject><foreignObject style="overflow: visible;" x="150.625" y="105" width="21.125" height="30"><span class="ct-label ct-horizontal" xmlns="http://www.w3.org/1999/xhtml">6</span></foreignObject><foreignObject style="overflow: visible;" x="171.75" y="105" width="21.125" height="30"><span class="ct-label ct-horizontal" xmlns="http://www.w3.org/1999/xhtml">7</span></foreignObject><foreignObject style="overflow: visible;" x="192.875" y="105" width="21.125" height="30"><span class="ct-label ct-horizontal" xmlns="http://www.w3.org/1999/xhtml">8</span></foreignObject><foreignObject style="overflow: visible;" y="85" x="-5" height="21.11111111111111" width="40"><span class="ct-label ct-vertical" xmlns="http://www.w3.org/1999/xhtml">0</span></foreignObject><foreignObject style="overflow: visible;" y="67.72727272727272" x="-5" height="21.11111111111111" width="40"><span class="ct-label ct-vertical" xmlns="http://www.w3.org/1999/xhtml">2</span></foreignObject><foreignObject style="overflow: visible;" y="50.45454545454545" x="-5" height="21.11111111111111" width="40"><span class="ct-label ct-vertical" xmlns="http://www.w3.org/1999/xhtml">4</span></foreignObject><foreignObject style="overflow: visible;" y="33.18181818181818" x="-5" height="21.11111111111111" width="40"><span class="ct-label ct-vertical" xmlns="http://www.w3.org/1999/xhtml">6</span></foreignObject><foreignObject style="overflow: visible;" y="15.909090909090907" x="-5" height="21.11111111111111" width="40"><span class="ct-label ct-vertical" xmlns="http://www.w3.org/1999/xhtml">8</span></foreignObject></g><g class="ct-grids"><line x1="45" x2="45" y1="5" y2="100" class="ct-grid ct-horizontal"></line><line x1="66.125" x2="66.125" y1="5" y2="100" class="ct-grid ct-horizontal"></line><line x1="87.25" x2="87.25" y1="5" y2="100" class="ct-grid ct-horizontal"></line><line x1="108.375" x2="108.375" y1="5" y2="100" class="ct-grid ct-horizontal"></line><line x1="129.5" x2="129.5" y1="5" y2="100" class="ct-grid ct-horizontal"></line><line x1="150.625" x2="150.625" y1="5" y2="100" class="ct-grid ct-horizontal"></line><line x1="171.75" x2="171.75" y1="5" y2="100" class="ct-grid ct-horizontal"></line><line x1="192.875" x2="192.875" y1="5" y2="100" class="ct-grid ct-horizontal"></line><line y1="100" y2="100" x1="45" x2="214" class="ct-grid ct-vertical"></line><line y1="82.72727272727272" y2="82.72727272727272" x1="45" x2="214" class="ct-grid ct-vertical"></line><line y1="65.45454545454545" y2="65.45454545454545" x1="45" x2="214" class="ct-grid ct-vertical"></line><line y1="48.18181818181818" y2="48.18181818181818" x1="45" x2="214" class="ct-grid ct-vertical"></line><line y1="30.909090909090907" y2="30.909090909090907" x1="45" x2="214" class="ct-grid ct-vertical"></line></g><g class="ct-series ct-series-a"><path d="M45,100L45,56.818C48.521,51.061,59.083,25.152,66.125,22.273C73.167,19.394,80.208,38.106,87.25,39.545C94.292,40.985,101.333,28.03,108.375,30.909C115.417,33.788,122.458,49.621,129.5,56.818C136.542,64.015,143.583,74.091,150.625,74.091C157.667,74.091,164.708,58.258,171.75,56.818C178.792,55.379,189.354,64.015,192.875,65.455L192.875,100" class="ct-area" values="5,9,7,8,5,3,5,4"></path><path d="M45,56.818C48.521,51.061,59.083,25.152,66.125,22.273C73.167,19.394,80.208,38.106,87.25,39.545C94.292,40.985,101.333,28.03,108.375,30.909C115.417,33.788,122.458,49.621,129.5,56.818C136.542,64.015,143.583,74.091,150.625,74.091C157.667,74.091,164.708,58.258,171.75,56.818C178.792,55.379,189.354,64.015,192.875,65.455" class="ct-line" values="5,9,7,8,5,3,5,4"></path><line x1="45" y1="56.81818181818182" x2="45.01" y2="56.81818181818182" class="ct-point" value="5"></line><line x1="66.125" y1="22.272727272727266" x2="66.135" y2="22.272727272727266" class="ct-point" value="9"></line><line x1="87.25" y1="39.54545454545455" x2="87.26" y2="39.54545454545455" class="ct-point" value="7"></line><line x1="108.375" y1="30.909090909090907" x2="108.385" y2="30.909090909090907" class="ct-point" value="8"></line><line x1="129.5" y1="56.81818181818182" x2="129.51" y2="56.81818181818182" class="ct-point" value="5"></line><line x1="150.625" y1="74.0909090909091" x2="150.635" y2="74.0909090909091" class="ct-point" value="3"></line><line x1="171.75" y1="56.81818181818182" x2="171.76" y2="56.81818181818182" class="ct-point" value="5"></line><line x1="192.875" y1="65.45454545454545" x2="192.885" y2="65.45454545454545" class="ct-point" value="4"></line></g></svg></div>
                        </div>
                    </div>
                </div>
            </li>
        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 603px; right: 3px;"><div class="ps-scrollbar-y" style="top: 0px; height: 277px;"></div></div></ul>
        <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only cyan"><i class="mdi-navigation-menu"></i></a>
        </aside>
      <!-- END LEFT SIDEBAR NAV-->

      <!-- //////////////////////////////////////////////////////////////////////////// -->

      <!-- START CONTENT -->
      <section id="content">

        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper" class=" grey lighten-3">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
          <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Forms Validation</h5>
                <ol class="breadcrumbs">
                  <li><a href="index.html">Dashboard</a>
                  </li>
                  <li><a href="#">Forms</a>
                  </li>
                  <li class="active">Forms Validation</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->


        <!--start container-->
        <div class="container">
          
          <!--
              jQuery Validation Plugin
          -->
          <p class="caption">jQuery Validation Plugin</p>
          <p><a href="http://jqueryvalidation.org/" target="_blank">jQuery Validation</a> This jQuery plugin makes simple clientside form validation easy, whilst still offering plenty of customization options.</p>
          <div class="divider"></div>
          <!--jqueryvalidation-->
          <div id="jqueryvalidation" class="section">
            <div class="row">
              <div class="col s12 m12 l12">
                  <div class="col s12 m12 l6">
                        <div class="card-panel">
                            <h4 class="header2">Validations example</h4>
                            <div class="row">
                                <form class="formValidate" id="formValidate" method="get" action="" novalidate="novalidate">
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label for="uname">Username*</label>
                                            <input id="uname" name="uname" type="text" data-error=".errorTxt1">
                                            <div class="errorTxt1"></div>
                                        </div>
                                        <div class="input-field col s12">
                                          <label for="cemail">E-Mail *</label>
                                          <input id="cemail" type="email" name="cemail" data-error=".errorTxt2">
                                          <div class="errorTxt2"></div>
                                        </div>
                                        <div class="input-field col s12">
                                          <label for="password">Password *</label>
                                          <input id="password" type="password" name="password" data-error=".errorTxt3">
                                          <div class="errorTxt3"></div>
                                        </div>
                                        <div class="input-field col s12">
                                          <label for="cpassword">Confirm Password *</label>
                                          <input id="cpassword" type="password" name="cpassword" data-error=".errorTxt4">
                                          <div class="errorTxt4"></div>
                                        </div>
                                        <div class="input-field col s12">
                                            <label for="curl">URL *</label>
                                            <input id="curl" type="url" name="curl" data-error=".errorTxt5">
                                            <div class="errorTxt5"></div>
                                        </div>
                                        <div class="col s12">
                                            <label for="crole">Role *</label>
                                            <select class="error browser-default" id="crole" name="crole" data-error=".errorTxt6">
                                                <option value="" disabled="" selected="">Choose your profile</option>
                                                <option value="1">Manager</option>
                                                <option value="2">Developer</option>
                                                <option value="3">Business</option>
                                            </select>
                                            <div class="input-field">
                                                <div class="errorTxt6"></div>
                                            </div>
                                        </div> 
                                        <div class="input-field col s12">
                                            <textarea id="ccomment" name="ccomment" class="materialize-textarea validate" data-error=".errorTxt7"></textarea>
                                            <label for="ccomment">Your comment *</label>
                                            <div class="errorTxt7"></div>
                                        </div>
                                        <div class="col s12">
                                            <label for="genter_select">Gender *</label>
                                            <p>
                                              <input name="cgender" type="radio" id="gender_male" data-error=".errorTxt8">
                                              <label for="gender_male">Male</label>
                                            </p>
                                            <p>
                                              <input name="cgender" type="radio" id="gender_female" value="f">
                                              <label for="gender_female">Female</label>
                                            </p>
                                            <div class="input-field">
                                                <div class="errorTxt8"></div>
                                            </div>
                                        </div>
                                        <div class="col s12">
                                            <label for="tnc_select">T&amp;C *</label>
                                            <p>
                                                <input type="checkbox" class="checkbox" id="cagree" name="cagree" data-error=".errorTxt9"> 
                                                <label for="cagree">Please agree to our policy</label>
                                             </p>
                                            <div class="input-field">
                                                <div class="errorTxt6"></div>
                                            </div>
                                        </div>
                                        <div class="input-field col s12">
                                            <button class="btn waves-effect waves-light right submit" type="submit" name="action">Submit
                                              <i class="mdi-content-send right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m12 l6">
                        <p>Simple jQuery Validation script to validate your form inputs.</p>
    <pre class=" language-javascript"><code class=" language-javascript"><span class="token lf">
</span>     <span class="token function">$</span><span class="token punctuation">(</span><span class="token string">"#formValidate"</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">validate</span><span class="token punctuation">(</span><span class="token punctuation">{</span><span class="token lf">
</span>        rules<span class="token punctuation">:</span> <span class="token punctuation">{</span><span class="token lf">
</span>            uname<span class="token punctuation">:</span> <span class="token punctuation">{</span><span class="token lf">
</span>                required<span class="token punctuation">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span><span class="token lf">
</span>                minlength<span class="token punctuation">:</span> <span class="token number">5</span><span class="token lf">
</span>            <span class="token punctuation">}</span><span class="token punctuation">,</span><span class="token lf">
</span>            cemail<span class="token punctuation">:</span> <span class="token punctuation">{</span><span class="token lf">
</span>                required<span class="token punctuation">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span><span class="token lf">
</span>                email<span class="token punctuation">:</span><span class="token boolean">true</span><span class="token lf">
</span>            <span class="token punctuation">}</span><span class="token punctuation">,</span><span class="token lf">
</span>            password<span class="token punctuation">:</span> <span class="token punctuation">{</span><span class="token lf">
</span><span class="token tab"> </span><span class="token tab"> </span><span class="token tab"> </span><span class="token tab"> </span>required<span class="token punctuation">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span><span class="token lf">
</span><span class="token tab"> </span><span class="token tab"> </span><span class="token tab"> </span><span class="token tab"> </span>minlength<span class="token punctuation">:</span> <span class="token number">5</span><span class="token lf">
</span><span class="token tab"> </span><span class="token tab"> </span><span class="token tab"> </span><span class="token punctuation">}</span><span class="token punctuation">,</span><span class="token lf">
</span><span class="token tab"> </span><span class="token tab"> </span><span class="token tab"> </span>cpassword<span class="token punctuation">:</span> <span class="token punctuation">{</span><span class="token lf">
</span><span class="token tab"> </span><span class="token tab"> </span><span class="token tab"> </span><span class="token tab"> </span>required<span class="token punctuation">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span><span class="token lf">
</span><span class="token tab"> </span><span class="token tab"> </span><span class="token tab"> </span><span class="token tab"> </span>minlength<span class="token punctuation">:</span> <span class="token number">5</span><span class="token punctuation">,</span><span class="token lf">
</span><span class="token tab"> </span><span class="token tab"> </span><span class="token tab"> </span><span class="token tab"> </span>equalTo<span class="token punctuation">:</span> <span class="token string">"#password"</span><span class="token lf">
</span><span class="token tab"> </span><span class="token tab"> </span><span class="token tab"> </span><span class="token punctuation">}</span><span class="token punctuation">,</span><span class="token lf">
</span><span class="token tab"> </span><span class="token tab"> </span><span class="token tab"> </span>curl<span class="token punctuation">:</span> <span class="token punctuation">{</span><span class="token lf">
</span>                required<span class="token punctuation">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span><span class="token lf">
</span>                url<span class="token punctuation">:</span><span class="token boolean">true</span><span class="token lf">
</span>            <span class="token punctuation">}</span><span class="token punctuation">,</span><span class="token lf">
</span>            crole<span class="token punctuation">:</span><span class="token string">"required"</span><span class="token punctuation">,</span><span class="token lf">
</span>            ccomment<span class="token punctuation">:</span> <span class="token punctuation">{</span><span class="token lf">
</span><span class="token tab"> </span><span class="token tab"> </span><span class="token tab"> </span><span class="token tab"> </span>required<span class="token punctuation">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span><span class="token lf">
</span><span class="token tab"> </span><span class="token tab"> </span><span class="token tab"> </span><span class="token tab"> </span>minlength<span class="token punctuation">:</span> <span class="token number">15</span><span class="token lf">
</span>            <span class="token punctuation">}</span><span class="token punctuation">,</span><span class="token lf">
</span>            cgender<span class="token punctuation">:</span><span class="token string">"required"</span><span class="token punctuation">,</span><span class="token lf">
</span><span class="token tab"> </span><span class="token tab"> </span><span class="token tab"> </span>cagree<span class="token punctuation">:</span><span class="token string">"required"</span><span class="token punctuation">,</span><span class="token lf">
</span>        <span class="token punctuation">}</span><span class="token punctuation">,</span><span class="token lf">
</span>        <span class="token comment" spellcheck="true">//For custom messages</span><span class="token lf">
</span>        messages<span class="token punctuation">:</span> <span class="token punctuation">{</span><span class="token lf">
</span>            uname<span class="token punctuation">:</span><span class="token punctuation">{</span><span class="token lf">
</span>                required<span class="token punctuation">:</span> <span class="token string">"Enter a username"</span><span class="token punctuation">,</span><span class="token lf">
</span>                minlength<span class="token punctuation">:</span> <span class="token string">"Enter at least 5 characters"</span><span class="token lf">
</span>            <span class="token punctuation">}</span><span class="token punctuation">,</span><span class="token lf">
</span>            curl<span class="token punctuation">:</span> <span class="token string">"Enter your website"</span><span class="token punctuation">,</span><span class="token lf">
</span>        <span class="token punctuation">}</span><span class="token punctuation">,</span><span class="token lf">
</span>        errorElement <span class="token punctuation">:</span> <span class="token string">'div'</span><span class="token punctuation">,</span><span class="token lf">
</span>        errorPlacement<span class="token punctuation">:</span> <span class="token keyword">function</span><span class="token punctuation">(</span>error<span class="token punctuation">,</span> element<span class="token punctuation">)</span> <span class="token punctuation">{</span><span class="token lf">
</span>          <span class="token keyword">var</span> placement <span class="token operator">=</span> <span class="token function">$</span><span class="token punctuation">(</span>element<span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">data</span><span class="token punctuation">(</span><span class="token string">'error'</span><span class="token punctuation">)</span><span class="token punctuation">;</span><span class="token lf">
</span>          <span class="token keyword">if</span> <span class="token punctuation">(</span>placement<span class="token punctuation">)</span> <span class="token punctuation">{</span><span class="token lf">
</span>            <span class="token function">$</span><span class="token punctuation">(</span>placement<span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">append</span><span class="token punctuation">(</span>error<span class="token punctuation">)</span><span class="token lf">
</span>          <span class="token punctuation">}</span> <span class="token keyword">else</span> <span class="token punctuation">{</span><span class="token lf">
</span>            error<span class="token punctuation">.</span><span class="token function">insertAfter</span><span class="token punctuation">(</span>element<span class="token punctuation">)</span><span class="token punctuation">;</span><span class="token lf">
</span>          <span class="token punctuation">}</span><span class="token lf">
</span>        <span class="token punctuation">}</span><span class="token lf">
</span>     <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span><span class="token lf">
</span>     </code></pre>
                    
              </div>
            </div>
          </div>
          
            
          </div>
          <div class="divider"></div>
          <!--
              Custom Error and Success Messages
          -->
          <p class="caption">Materialize default error and success messages</p>
          <p>Materialize admin theme has built in custom error or success messages, You can add custom validation messages by adding either <code class="  language-markup">data-error</code> or <code class="  language-markup">data-success</code> attributes to your input field labels.</p>
          
          <div class="divider"></div>
          <div id="default-validation" class="section">
              <div class="row">
                  <!--Form validation with placeholder-->
                  <div class="col s12 m12 l6">
                    <div class="card-panel">
                        <h4 class="header2">Form validation with placeholder</h4>
                        <p>Add class <code class="  language-markup">.left-alert</code> to show error message in left align.</p>
                        <!--add .left-alert to show messages left side-->
                        <div class="row">
                            <form class="col s12 left-alert">
                              <div class="row">
                                  <div class="input-field col s12">
                                    <input id="first_input1" type="text" class="validate" placeholder="John Doe">
                                    <label for="first_input1" data-error="Please enter first name." class="active">First Name</label>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="input-field col s12">
                                    <input id="age_input1" type="number" class="validate" placeholder="25">
                                    <label for="age_input1" data-error="Please enter your age." class="active">Age</label>
                                  </div>
                              </div>
                              <div class="row">
                                <div class="input-field col s12">
                                  <input id="email_input1" type="email" class="validate" placeholder="john@domainname.com">
                                  <label for="email_input1" class="active" data-error="Please enter valid email." data-success="I Like it!">Email</label>
                                </div>
                              </div>
                              <div class="row">
                                <div class="input-field col s12">
                                  <input id="url_input1" type="url" class="validate" placeholder="http://geekslabs.com/">
                                  <label for="url_input1" class="active" data-error="Please enter valid url.">URL</label>
                                </div>
                              </div>
                              <div class="row">
                                <div class="input-field col s12">
                                  <input id="password1" type="password" class="validate" placeholder="MonKey&amp;420@">
                                  <label for="password1" class="active">Password</label>
                                </div>
                              </div>
                              <div class="row">
                                <div class="input-field col s12">
                                  <textarea id="message1" class="materialize-textarea validate" length="120" placeholder="Oh WoW! Let me check this one too."></textarea>
                                  <label for="message1" class="active">Message</label>
                                <span class="character-counter" style="float: right; font-size: 12px; height: 1px;"></span></div>
                             </div>                  
                            <div class="row">
                              <div class="input-field col s12">
                                <button class="btn waves-effect waves-light right" type="submit" name="action">Submit
                                  <i class="mdi-content-send right"></i>
                                </button>
                              </div>
                            </div>
                        </form>
                        </div>
                    </div>
                  </div>
                  <!--Form validation with icons-->
                  <div class="col s12 m12 l6">
                    <div class="card-panel">
                        <h4 class="header2">Form validation with icons</h4>
                        <p>Add class <code class="  language-markup">.right-alert</code> to show error message in right align.</p>
                        <!--add .right-alert to show messages right side-->
                        <div class="row">
                            <form class="col s12 right-alert">
                              <div class="row">
                                  <div class="input-field col s12">
                                      <i class="mdi-action-account-circle prefix"></i>
                                    <input id="first_input2" type="text" class="validate">
                                    <label for="first_input2" data-error="Please enter first name." data-success="Perfect!">First Name</label>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="input-field col s12">
                                    <i class="mdi-action-verified-user prefix"></i>
                                    <input id="age_input2" type="number" class="validate">
                                    <label for="age_input2" data-error="Please enter your age." data-success="Wow!">Age</label>
                                  </div>
                              </div>
                              <div class="row">
                                <div class="input-field col s12">
                                  <i class="mdi-communication-email prefix"></i>
                                  <input id="email_input2" type="email" class="validate">
                                  <label for="email_input2" class="" data-error="Please enter valid email." data-success="I Like it!">Email</label>
                                </div>
                              </div>
                              <div class="row">
                                <div class="input-field col s12">
                                  <i class="mdi-content-link prefix"></i>
                                  <input id="url_input2" type="url" class="validate">
                                  <label for="url_input2" class="" data-error="Please enter valid url." data-success="I know you!">URL</label>
                                </div>
                              </div>
                              <div class="row">
                                <div class="input-field col s12">
                                  <i class="mdi-action-lock-outline prefix"></i>
                                  <input id="password2" type="password" class="validate">
                                  <label for="password2">Password</label>
                                </div>
                              </div>
                              <div class="row">
                                <div class="input-field col s12">
                                  <i class="mdi-action-question-answer prefix"></i>
                                  <textarea id="message2" class="materialize-textarea validate" length="120"></textarea>
                                  <label for="message2" class="">Message</label>
                                <span class="character-counter" style="float: right; font-size: 12px; height: 1px;"></span></div>
                             </div>                  
                            <div class="row">
                              <div class="input-field col s12">
                                <button class="btn waves-effect waves-light right" type="submit" name="action">Submit
                                  <i class="mdi-content-send right"></i>
                                </button>
                              </div>
                            </div>
                        </form>
                        </div>
                    </div>
                  </div>
              </div>
              <small><em>Note:</em> Materialize default validation work based on HTML5 input types.</small>
          </div>
          
          
          <!-- Floating Action Button -->
            <div class="fixed-action-btn" style="bottom: 50px; right: 19px;">
                <a class="btn-floating btn-large">
                  <i class="mdi-action-stars"></i>
                </a>
                <ul>
                  <li><a href="css-helpers.html" class="btn-floating red" style="transform: scaleY(0.4) scaleX(0.4) translateY(40px) translateX(0px); opacity: 0;"><i class="large mdi-communication-live-help"></i></a></li>
                  <li><a href="app-widget.html" class="btn-floating yellow darken-1" style="transform: scaleY(0.4) scaleX(0.4) translateY(40px) translateX(0px); opacity: 0;"><i class="large mdi-device-now-widgets"></i></a></li>
                  <li><a href="app-calendar.html" class="btn-floating green" style="transform: scaleY(0.4) scaleX(0.4) translateY(40px) translateX(0px); opacity: 0;"><i class="large mdi-editor-insert-invitation"></i></a></li>
                  <li><a href="app-email.html" class="btn-floating blue" style="transform: scaleY(0.4) scaleX(0.4) translateY(40px) translateX(0px); opacity: 0;"><i class="large mdi-communication-email"></i></a></li>
                </ul>
            </div>
            <!-- Floating Action Button -->
        </div>
        <!--end container-->

      </section>
      <!-- END CONTENT -->

      <!-- //////////////////////////////////////////////////////////////////////////// -->
      <!-- START RIGHT SIDEBAR NAV-->
      <aside id="right-sidebar-nav">
        <ul id="chat-out" class="side-nav rightside-navigation right-aligned ps-container ps-active-y" style="width: 300px; right: -310px; height: 727px;">
            <li class="li-hover">
            <a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
            <div id="right-search" class="row">
                <form class="col s12">
                    <div class="input-field">
                        <i class="mdi-action-search prefix"></i>
                        <input id="icon_prefix" type="text" class="validate">
                        <label for="icon_prefix">Search</label>
                    </div>
                </form>
            </div>
            </li>
            <li class="li-hover">
                <ul class="chat-collapsible" data-collapsible="expandable">
                <li class="active">
                    <div class="collapsible-header teal white-text active"><i class="mdi-social-whatshot"></i>Recent Activity</div>
                    <div class="collapsible-body recent-activity" style="display: block;">
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-add-shopping-cart"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">just now</a>
                                <p>Jim Doe Purchased new equipments for zonal office.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-device-airplanemode-on"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">Yesterday</a>
                                <p>Your Next flight for USA will be on 15th August 2015.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">5 Days Ago</a>
                                <p>Natalya Parker Send you a voice mail for next conference.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-store"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">Last Week</a>
                                <p>Jessy Jay open a new store at S.G Road.</p>
                            </div>
                        </div>
                        <div class="recent-activity-list chat-out-list row">
                            <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                            </div>
                            <div class="col s9 recent-activity-list-text">
                                <a href="#">5 Days Ago</a>
                                <p>Natalya Parker Send you a voice mail for next conference.</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="active">
                    <div class="collapsible-header light-blue white-text active"><i class="mdi-editor-attach-money"></i>Sales Repoart</div>
                    <div class="collapsible-body sales-repoart" style="display: block;">
                        <div class="sales-repoart-list  chat-out-list row">
                            <div class="col s8">Target Salse</div>
                            <div class="col s4"><span id="sales-line-1"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Payment Due</div>
                            <div class="col s4"><span id="sales-bar-1"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Total Delivery</div>
                            <div class="col s4"><span id="sales-line-2"></span>
                            </div>
                        </div>
                        <div class="sales-repoart-list chat-out-list row">
                            <div class="col s8">Total Progress</div>
                            <div class="col s4"><span id="sales-bar-2"></span>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header red white-text"><i class="mdi-action-stars"></i>Favorite Associates</div>
                    <div class="collapsible-body favorite-associates">
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Eileen Sideways</p>
                                <p class="place">Los Angeles, CA</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Zaham Sindil</p>
                                <p class="place">San Francisco, CA</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Renov Leongal</p>
                                <p class="place">Cebu City, Philippines</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Weno Carasbong</p>
                                <p>Tokyo, Japan</p>
                            </div>
                        </div>
                        <div class="favorite-associate-list chat-out-list row">
                            <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                            </div>
                            <div class="col s8">
                                <p>Nusja Nawancali</p>
                                <p class="place">Bangkok, Thailand</p>
                            </div>
                        </div>
                    </div>
                </li>
                </ul>
            </li>
        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 667px; right: 3px;"><div class="ps-scrollbar-y" style="top: 0px; height: 611px;"></div></div></ul>
      </aside>
      <!-- LEFT RIGHT SIDEBAR NAV-->

    </div>
    <!-- END WRAPPER -->

  </div>
  <!-- END MAIN -->



  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START FOOTER -->
  <footer class="page-footer">
    <div class="footer-copyright">
      <div class="container">
        <span>Copyright © 2015 <a class="grey-text text-lighten-4" href="http://themeforest.net/user/geekslabs/portfolio?ref=geekslabs" target="_blank">GeeksLabs</a> All rights reserved.</span>
        <span class="right"> Design and Developed by <a class="grey-text text-lighten-4" href="http://geekslabs.com/">GeeksLabs</a></span>
        </div>
    </div>
  </footer>
    <!-- END FOOTER -->



    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>    
    <!--angularjs-->
    <script type="text/javascript" src="js/plugins/angular.min.js"></script>
    <!--materialize js-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!--prism -->
    <script type="text/javascript" src="js/plugins/prism/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="js/plugins/chartist-js/chartist.min.js"></script>
    
    <!-- chartist -->
    <script type="text/javascript" src="js/plugins/jquery-validation/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/plugins/jquery-validation/additional-methods.min.js"></script>
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="js/custom-script.js"></script>
    
    <script type="text/javascript">
    $("#formValidate").validate({
        rules: {
            uname: {
                required: true,
                minlength: 5
            },
            cemail: {
                required: true,
                email:true
            },
            password: {
                required: true,
                minlength: 5
            },
            cpassword: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            curl: {
                required: true,
                url:true
            },
            crole:"required",
            ccomment: {
                required: true,
                minlength: 15
            },
            cgender:"required",
            cagree:"required",
        },
        //For custom messages
        messages: {
            uname:{
                required: "Enter a username",
                minlength: "Enter at least 5 characters"
            },
            curl: "Enter your website",
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          if (placement) {
            $(placement).append(error)
          } else {
            error.insertAfter(element);
          }
        }
     });
    </script>
    


<div class="hiddendiv common"></div><div class="drag-target" style="left: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div><div class="drag-target" style="right: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div></body></html>