@extends('layouts.main')

@section('navbar')
<header id="header" class="page-topbar">
    <div class="navbar-fixed">
        <nav class="grey darken-3">
            <div class="nav-wrapper">
                <a href="/" class="hide-on-med-and-down" style="margin-left: 260px;font-weight: bold;">Sistem Informasi Pengelolaan ATK</a>
                <ul class="right hide-on-med-and-down">
                    <li><a href="{{ route('logout') }}" class="tooltipped" data-position="bottom" data-delay="10" data-tooltip="Logout"><i class="mdi-hardware-keyboard-tab"></i></a></li>
                    <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen tooltipped" data-position="bottom" data-delay="10" data-tooltip="Fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>
@endsection

@section('sidebar')
<aside id="left-sidebar-nav">
    <ul id="slide-out" class="side-nav fixed leftside-navigation collapsible collapsible-accordion">
    	<li class="bold"><a href="{{ route('index') }}" class="waves-effect waves-cyan"><img src="{{ asset('assets/images/logobi.png') }}"></a>
        </li>
        <li class="user-details cyan darken-2">
            <div class="row">
                <div class="col col s12 m12 l12">
                    <ul id="profile-dropdown" class="dropdown-content">
                        <li><a href="{{ route('logout') }}"><i class="mdi-hardware-keyboard-tab"></i> Logout</a></li>
                    </ul>
                    <a class="btn-flat dropdown-button waves-effect waves-light grey-text text-darken-4 profile-btn" style="font-weight:bold;text-shadow:none;" href="#" data-activates="profile-dropdown">{{ session('name') }}<i class="mdi-navigation-arrow-drop-down right"></i></a>
                    <p class="user-roal grey-text text-darken-4" style="font-weight:bold;line-height:20px;text-shadow:none;">{{ $whoami->division }}</p>
                    <p class="user-roal grey-text text-darken-4" style="font-weight:bold;text-shadow:none;">
                        @if ($manager && $operator)
                            (Admin)
                        @elseif ($operator)
                            (Operator)
                        @elseif ($manager)
                            (Manajer)
                        @else
                            (Pengguna)
                        @endif
                    </p>
                </div>
            </div>
        </li>
        @if ($operator)
            <li><a href="{{ route('index') }}" class="waves-effect waves-cyan"><i class="mdi-editor-insert-chart"></i> Dashboard</a></li>
            <li><a href="{{ route('cat-index') }}" class="waves-effect waves-cyan"><i class="mdi-device-now-widgets"></i> Kategori ATK</a></li>
            <li><a href="{{ route('inventory-index') }}" class="waves-effect waves-cyan"><i class="mdi-device-now-widgets"></i> ATK</a></li>
            <li><a href="{{ route('request-index') }}" class="waves-effect waves-cyan"><i class="mdi-action-shopping-cart"></i> Permintaan <span class="new badge red darken-3">@yield('new_request')</span></a></li>
        @elseif ($manager && (session('username') == 'baharipri' || session('username') == 'marta')) 
            <li><a href="{{ route('index') }}" class="waves-effect waves-cyan"><i class="mdi-editor-insert-chart"></i> Dashboard</a></li>
            <li><a href="{{ route('inventory-index') }}" class="waves-effect waves-cyan"><i class="mdi-device-now-widgets"></i> ATK</a></li>
            <li><a href="{{ route('request-index') }}" class="waves-effect waves-cyan"><i class="mdi-toggle-check-box"></i> Permintaan <span class="new badge red darken-3">@yield('new_request')</span></a></li>
            <li><a href="{{ route('admin-index') }}" class="waves-effect waves-cyan"><i class="mdi-social-people"></i> Pengguna</a></li>
        @elseif ($manager)
            <li><a href="{{ route('index') }}" class="waves-effect waves-cyan"><i class="mdi-editor-insert-chart"></i> Dashboard</a></li>
            <li><a href="{{ route('request-create') }}" class="waves-effect waves-cyan"><i class="mdi-content-add-box"></i> Buat Permintaan</a></li>
            <li><a href="{{ route('request-index-user', session('username')) }}" class="waves-effect waves-cyan"><i class="mdi-toggle-check-box"></i> Permintaan <span class="new badge red darken-3">@yield('new_request')</span></a></li>
        @else
            <li><a href="{{ route('request-create') }}" class="waves-effect waves-cyan"><i class="mdi-content-add-box"></i> Buat Permintaan</a></li>
            <li><a href="{{ route('request-index-user', session('username')) }}" class="waves-effect waves-cyan"><i class="mdi-toggle-check-box"></i> Riwayat Permintaan</a></li>
        @endif
    </ul>
    <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only darken-2"><i class="mdi-navigation-menu light indigo darken-4" ></i></a>
</aside>
@endsection

@section('content')
<div id="main">
    <div class="wrapper">
        @yield('sidebar')
        <section id="content">
            @if ($operator)
                <!--BREADCRUMBS-->
                <div id="breadcrumbs-wrapper" class="grey lighten-3">
                    <div class="container">
                        <div class="row">
                            <div class="col s12 m12 l12">
                                <h5 class="breadcrumbs-title">@yield('title')</h5>
                                <ol class="breadcrumb">
                                    @yield('root')
                                    @yield('previous')
                                    <li class="active">@yield('here')</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            <!--MAIN CONTENT-->
            <div class="container">
                <div class="section">
                    @yield('admin-content')
                </div>
            </div>
    </div>
</div>
</section>
@endsection

@section('footer')
<footer class="page-footer grey darken-3">
    <div class="footer-copyright grey darken-3">
        <div class="container">
           © 2016 <a class="grey-text text-lighten-4" href="http://www.bi.go.id" target="_blank">Bank Indonesia</a>
           <a class="grey-text text-lighten-4" style="font-size:10px;">by Khairunisa&Khairunnisa</a>
        </div>
    </div>
</footer>
@endsection

@section('scripts')
<script>
    $(function() {
         var pgurl = window.location.href.substr(window.location.href.lastIndexOf("/")+1);
         $(".side-nav li a").each(function(){
              if($(this).attr("href") == pgurl || $(this).attr("href") == '' )
              $(this).addClass("active");
         })
    });
</script>
@endsection