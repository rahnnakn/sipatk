@extends('layouts.admin')
@section('page-title', 'Bon Permintaan | ')
@section('title', 'Bon Permintaan')
@section('root') 
  <li><a class="blue-text text-darken-4" href="{{ route('index') }}">Dashboard</a></li>
@endsection
@section('previous')
  <li><a class="blue-text text-darken-4" href="{{ route('request-index') }}">Daftar Permintaan</a></li>
@endsection
@section('here', 'Bon Permintaan')
@section('styles')
  <style>
    p, ul li {
      font-size: 20px;
    }
  </style>
@endsection
@if ($operator || $manager)
  @section('new_request', $count_requests)
@endif
@section('admin-content')
    <div class="col s12 m4 l4">
      <div class="right-align">
        @if ($request->status == 1 && $manager && session('username') == $request->approver) 
          <a href="{{ route('request-approve-div', $request->id) }}" class="btn waves-effect waves-light green darken-4 approve tooltipped backdrop1" data-position="bottom" data-delay="10" data-tooltip="Setuju" style="margin-right:20px;"><i class="mdi-action-thumb-up"></i></a>
          <a href="#reject-byDiv" class="btn waves-effect waves-light red darken-4 modal-trigger tooltipped backdrop2" data-position="bottom" data-delay="10" data-tooltip="Tolak" style="margin-right:20px;"><i class="mdi-action-thumb-down"></i></a>
        @elseif ($request->status == 2 && $manager && (session('username') == 'baharipri' || session('username') == 'marta')) 
          <a href="{{ route('request-approve-sla', $request->id) }}" class="btn waves-effect waves-light green darken-4 approve tooltipped" data-position="bottom" data-delay="10" data-tooltip="Setuju" style="margin-right:20px;"><i class="mdi-action-thumb-up"></i></a>
          <a href="#reject-bySLA" class="btn waves-effect waves-light red darken-4 modal-trigger tooltipped" data-position="bottom" data-delay="10" data-tooltip="Tolak" style="margin-right:20px;"><i class="mdi-action-thumb-down"></i></a>
        @elseif ($request->status == 3 && $operator) 
          <a href="{{ route('request-process', $request->id) }}" class="btn waves-effect waves-light indigo darken-4 process tooltipped" data-position="bottom" data-delay="10" data-tooltip="Proses" style="margin-right:20px;"><i class="mdi-action-cached"></i></a>
        @elseif ($request->status == 4 && $operator) 
          <a href="#finish-request" class="btn waves-effect waves-light indigo darken-4 modal-trigger tooltipped" data-position="bottom" data-delay="10" data-tooltip="Selesai" style="margin-right:20px;"><i class="mdi-action-done"></i></a>
          <a href="{{ route('request-update', $request->id) }}" class="btn waves-effect waves-light blue" style="margin-right:20px;"><i class="mdi-editor-border-color"></i></a>
        @endif
        <a href="{{ route('request-export', $request->id) }}" class="btn waves-effect waves-light blue tooltipped" data-position="bottom" data-delay="1" data-tooltip="Cetak Bon Permintaan"><i class="mdi-action-print"></i></a>
      </div>
      <br>
    </div>
    <div class="col s12 m12 l12">
      <div class="card">
        <div class="container">
          <div class="container">
            <br>
          <h3 class="header center">Bon Permintaan #{{ $request->id }}</h3>
          @if (!$manager && !$operator && $request->status == 4)
            <h5 class="header">Kode Pengambilan: {{ $request->ticket }}</h5>
          @endif
          <div class="row centered">
            <div class="col s12 m12 l12" style="margin-bottom:20px;">
              @if (session()->has('flash_message'))
                <div id="card-alert" class="card cyan lighten-5">
                    <div class="card-content cyan-text darken-1">
                        <p class="single-alert">{{ session('flash_message') }}</p>
                    </div>
                </div>
                @endif
                <div id="card-alert" class="card blue darken-5">
                    <div class="card-content white-text">
                        <p class="single-alert">
                          @if ($request->status == 2)
                            Permintaan ini telah disetujui oleh {{ $request->u_approver->name }}, menanti persetujuan SLA.
                          @elseif ($request->status == 3)
                            Permintaan ini telah disetujui oleh SLA, menanti untuk diproses.
                          @elseif ($request->status == 4)
                            Permintaan ini sedang diproses.
                          @elseif ($request->status == 5)
                            Permintaan ini telah selesai diproses.
                          @elseif ($request->status == 0 && $request->approvedByDiv == 0)
                            Permintaan ini ditolak oleh kepala divisi.
                          @elseif ($request->status == 0)
                            Permintaan ini ditolak oleh SLA.
                          @else
                            Sedang menanti persetujuan {{ $request->u_approver->name }}.
                          @endif
                        </p>
                    </div>
                  </div>
                  <div class="col s12 m3 l6">
                    <h5>Permintaan oleh:</h5>
                    <hr>
                    <p>{{ $request->u_sender->name }} / {{ $request->u_sender->division }}</span></p>
                    <br>
                    <div class="col s12 m12 l12 cyan lighten-5">
                    @if ($request->admin_note != '')
                      <h5>Catatan:</h5>
                      <hr>
                      <p>{{ $request->admin_note }}</p>
                    @endif
                    </div>
                  </div>
                  <div class="col s12 m9 l6">
                    <h5>Nama ATK/Jumlah:</h5>
                    <hr>
                    <p>
                      <ul>
                        @foreach ($inventories as $inventory)
                        @if ($operator)
                          <li style="list-style-type:circle; margin-left: 25px;"><a href="{{ route('card-detail', $inventory->inventory_id) }}">{{ $inventory->inventory->name }}</a> / {{ $inventory->quantity }} {{ $inventory->unit }}</span></li>
                        @else
                          <li style="list-style-type:circle; margin-left: 25px;">{{ $inventory->inventory->name }} / {{ $inventory->quantity }} {{ $inventory->unit }}</span></li>
                        @endif
                        @endforeach
                      </ul>
                    </p>
                    <br>
                    <h5>Untuk keperluan:</h5>
                    <hr>
                    <p>{{ $request->purpose }}</span></p>
                    <br>
                    <h5>Melalui persetujuan:</h5>
                    <hr>
                    <p>{{ $request->u_approver->name }}</span></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="finish-request" class="modal">
      <form class="row formValidate" id="formValidate" novalidate="novalidate" method="post" action="{{ route('request-finish', $request->id) }}">
        <div class="modal-content">
          <h4>Kode Pengambilan</h4>
          {{ csrf_field() }}
          <div class="input-field col s12 m12 l12 validate">
            <label for="ticket">Masukkan kode di sini*</label>
            <input type="text" name="ticket" data-error=".errorTicket">
            <div class="errorTicket"></div>
          </div>
          <div class="input-field col s12 m12 l12">
          </div>
        </div>
        <div class="modal-footer">
          <a href="#!" class=" modal-action modal-close waves-effect waves-blue btn-flat">Batal</a>
          <button class="modal-action btn waves-effect waves-light indigo darken-4">Simpan</button>
        </div>
      </form>
    </div>

    <div id="reject-byDiv" class="modal">
      <form class="row formValidate" id="formValidate" novalidate="novalidate" method="post" action="{{ route('request-reject-div', $request->id) }}">
        <div class="modal-content">
          <h4>Alasan Penolakan</h4>
          {{ csrf_field() }}
          <div class="input-field col s12 m12 l12 validate">
            <label for="admin_note">Silakan berikan alasan penolakan*</label>
            <input type="text" name="admin_note" data-error=".errorNote">
            <div class="errorNote"></div>
          </div>
          <div class="input-field col s12 m12 l12">
          </div>
        </div>
        <div class="modal-footer">
          <a href="#!" class=" modal-action modal-close waves-effect waves-blue btn-flat">Batal</a>
          <button class="modal-action btn waves-effect waves-light indigo darken-4">Simpan</button>
        </div>
      </form>
    </div>

    <div id="reject-bySLA" class="modal">
      <form class="row formValidate" id="formValidate" novalidate="novalidate" method="post" action="{{ route('request-reject-sla', $request->id) }}">
        <div class="modal-content">
          <h4>Alasan Penolakan</h4>
          {{ csrf_field() }}
          <div class="input-field col s12 m12 l12 validate">
            <label for="admin_note">Silakan berikan alasan penolakan*</label>
            <input type="text" name="admin_note" data-error=".errorNote">
            <div class="errorNote"></div>
          </div>
          <div class="input-field col s12 m12 l12">
          </div>
        </div>
        <div class="modal-footer">
          <a href="#!" class=" modal-action modal-close waves-effect waves-blue btn-flat">Batal</a>
          <button class="modal-action btn waves-effect waves-light indigo darken-4 finish">Simpan</button>
        </div>
      </form>
    </div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script>
    $("#formValidate").validate({
        rules: {
            'ticket': {
                required: true,
            },
            admin_note: {
              required: true,
            }
    },
        //For custom messages
        messages: {
            'ticket':{
                required: "Kode pengambilan harus diisi",
            },
            admin_note: {
                required: "Alasan penolakan harus diisi",
            }

        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          if (placement) {
            $(placement).append(error)
          } else {
            error.insertAfter(element);
          }
        }
     });
</script>
@endsection