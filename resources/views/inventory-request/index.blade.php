@extends('layouts.admin')
@section('page-title', 'Permintaan | ')
@section('title', 'Permintaan')
@section('root') 
  <li><a class="blue-text text-darken-4" href="{{ route('index') }}">Dashboard</a></li>
@endsection
@section('here', 'Daftar Permintaan')
@section('styles')
<link rel="stylesheet" href="http://cdn.datatables.net/1.10.6/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('assets/js/plugins/data-tables/css/jquery.dataTables.min.css') }}">
@endsection
@if ($operator || $manager)
  @section('new_request', $count_requests)
@endif
@section('admin-content')
<div class="container col s12 m8 l9">
  <div class="right">
    <a href="{{ route('request-create') }}" class="btn waves-effect waves-light indigo darken-4 tooltipped" data-position="bottom" data-delay="1" data-tooltip="Tambah Permintaan ATK"><i class="mdi-content-add"></i></a>
  </div>
  <div id="table-datatables">
    <h4 class="header">Daftar Permintaan</h4>
    <div class="row">
      <div class="col s12 m12 l12">
        @if (session()->has('flash_message'))
          <div id="card-alert" class="card blue darken-1">
            <div class="card-content white-text darken-1">
                <p class="single-alert">{{ session('flash_message') }}</p>
            </div>
          </div>
        @endif
        <table id="data-table-simple" class="responsive-table display centered" cellspacing="0">
          <thead>
              <tr>
                  <th>Permintaan Oleh</th>
                  <th>Status</th>
                  <th>Operator</th>
                  <th>Tanggal Dibuat</th>
                  <th></th>
              </tr>
          </thead>
          <tfoot>
              <tr>
                  <th>Permintaan Oleh</th>
                  <th>Status</th>
                  <th>Operator</th>
                  <th>Tanggal Dibuat</th>
                  <th></th>
              </tr>
          </tfoot>
          <tbody>
            @foreach ($requests as $request)
            <tr>
              <td>{{ $request->u_sender->name }}</td>
              <td>
                <!-- 
                Status convention
                0 = Masuk
                1 = Disetujui Manajer Divisi
                2 = Disetujui SLA
                3 = Diproses
                4 = Selesai 
                -->
                @if ($request->status == 1)
                  Masuk
                @elseif ($request->status == 2)
                  Disetujui Manajer Divisi
                @elseif ($request->status == 3)
                  Disetujui SLA
                @elseif ($request->status == 4)
                  Diproses
                @elseif ($request->status == 5)
                  Selesai
                @else
                  Ditolak
                @endif
              </td>
              <td>{{ $request->u_receiver == null ? '-' : $request->u_receiver->name}}</td>
              <td>{{ $request->created_at }}</td>
              <td>
                <a href="{{ route('request-detail', $request->id) }}" class="btn waves-effect waves-light light-blue darken-4 tooltipped" data-position="bottom" data-delay="1" data-tooltip="Lihat Rincian Permintaan"><i class="mdi-action-list"></i></a>
                @if ($request->status == 3 && $operator) 
                  <a href="{{ route('request-process', $request->id) }}" class="btn waves-effect waves-light indigo darken-4 process tooltipped" data-position="bottom" data-delay="10" data-tooltip="Proses"><i class="mdi-action-cached"></i></a>
                @elseif ($request->status == 4 && $operator)
                  <a href="{{ route('request-update', $request->id) }}" class="btn waves-effect waves-light blue tooltipped" data-position="bottom" data-delay="1" data-tooltip="Ubah Rincian Permintaan"><i class="mdi-editor-border-color"></i></a>
                @endif
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('assets/js/plugins/data-tables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/data-tables/data-tables-script.js') }}"></script>
@endsection