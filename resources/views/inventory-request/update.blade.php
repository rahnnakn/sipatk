@extends('layouts.admin')
@section('page-title', 'Ubah Bon Permintaan | ')
@section('title', 'Ubah Bon Permintaan')
@section('root') 
  <li><a class="blue-text text-darken-4" href="{{ route('index') }}">Dashboard</a></li>
@endsection
@section('previous')
  <li><a class="blue-text text-darken-4" href="{{ route('request-index') }}">Daftar Permintaan</a></li>
@endsection
@section('here', 'Ubah Bon Permintaan')
@if ($operator || $manager)
  @section('new_request', $count_requests)
@endif
@section('admin-content')
    <div class="col s12 m8 l9">
      	<div class="card">
			<div id="jqueryvalidation" class="section" style="margin-bottom:20px;">
		        <div class="container">
		            <h2 class="header">Bon Permintaan #{{ $a_request->id }}</h2>
	            	<div id="card-alert" class="card blue darken-1">
						<div class="card-content white-text darken-1">
							<p>Area dengan (*) wajib diisi.</p>	
							@if (session()->has('flash_message'))
								<p class="single-alert">{{ session('flash_message') }}</p>
						    @endif
						</div>
		            </div>
		            <br>
		            <form id="formValidate" class="row container formValidate" method="post" action="{{ route('request-update', $a_request->id) }}">
		            	{{ csrf_field() }}
						<div class="col s12 m3 l3">
							<label for="author">Permintaan oleh</label>
							<input type="text" name="author" value="{{ $a_request->u_sender->name }}" disabled>
						</div>
						<div class="col s12 m6 l6">
							<label for="division">Divisi asal</label>
							<input type="text" name="division" value="{{ $a_request->u_sender->division }}" disabled>
						</div>
						<div class="col s12 m3 l3">
							<label for="approver">Yang Menyetujui</label>
							<input type="text" name="approver" value="{{ $a_request->u_approver->name }}" disabled>
						</div>
						<div class="col s12 m12 l12">
							<label for="purpose">Untuk Keperluan</label>
							<input type="text" name="purpose" value="{{ $a_request->purpose }}" disabled>
						</div>
						@foreach ($inventories as $inventory)
							<div class="col s11 m5 l5">
								<label for="inventory_name">Nama ATK</label>
								<input type="text" name="inventory_name[]" value="{{ $inventory->inventory->name }}" disabled>
							</div>
							<div class="col s1 m1 l1 slash"><br>/</div>
							<div class="col s6 m3 l3">
								<label for="quantity[]">Jumlah ATK</label>
								<input type="text" name="quantity[]" value="{{ $inventory->quantity }}" data-error=".errorQuantity">
								<div class="errorQuantity"></div>
							</div>
							<div class="col s6 m3 l3 validate">
								<label for="unit[]">Satuan Jumlah</label>
								<select name="unit[]">
									@foreach ($units as $unit)
										<option value="{{ $unit->unit }}" {{ $unit->unit == $inventory->unit ? 'selected' : '' }}>{{ $unit->unit }}</option>
									@endforeach
								</select>
							</div>
						@endforeach
						<div class="col s12 m12 l12 validate">
						<label for="admin_note">Catatan Operator</label>
							<input type="text" name="admin_note" data-error=".errorNotes" value="{{ $a_request->admin_note }}">
							<div class="errorNotes"></div>
						</div>
						<div class="col s12 m12 l12">
							<button class="btn waves-effect waves-light indigo darken-4 update-request">Simpan</button>
						</div>
		            </form>
		        </div>
	        </div>
      	</div>
    </div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script>
    $("#formValidate").validate({
        rules: {
            'quantity[]': {
                required: true,
                digits: true,
            },
           	'unit[]': {
                required: true,
            },
		},
        //For custom messages
        messages: {
            'quantity[]':{
                required: "Jumlah ATK harus diisi",
                digits: "Jumlah ATK harus berupa angka",
            },
            'unit[]':{
                required: "Satuan ATK harus diisi",
            },
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          if (placement) {
            $(placement).append(error)
          } else {
            error.insertAfter(element);
          }
        }
     });
</script>
@endsection
