@extends('layouts.admin')
@section('page-title', 'Daftar ATK | ')
@section('title', 'Daftar ATK')
@section('root') 
  <li><a class="blue-text text-darken-4" href="{{ route('index') }}">Dashboard</a></li>
@endsection
@section('here', 'Daftar ATK')
@section('styles')
<link rel="stylesheet" href="http://cdn.datatables.net/1.10.6/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('assets/js/plugins/data-tables/css/jquery.dataTables.min.css') }}">
@endsection
@if ($operator || $manager)
  @section('new_request', $count_requests)
@endif
@section('admin-content')
<div class="container col s12 m8 l9">
  <div class="right">
    @if ($operator)
      <a href="{{ route('inventory-create') }}" class="btn waves-effect waves-light indigo darken-4 tooltipped" data-position="bottom" data-delay="1" data-tooltip="Tambah ATK"><i class="mdi-content-add"></i></a>
    @endif
    <a href="{{ route('inventory-recap') }}" class="btn waves-effect waves-light blue tooltipped" data-position="bottom" data-delay="1" data-tooltip="Cetak Rekapitulasi Tahunan"><i class="mdi-action-print"></i></a>
  </div>
  <div id="table-datatables">
    <h4 class="header">Daftar ATK</h4>
    <div class="row">
      <div class="col s12 m12 l12">
        @if (session()->has('flash_message'))
            <div id="card-alert" class="card blue darken-1">
              <div class="card-content white-text darken-1">
                  <p class="single-alert">{{ session('flash_message') }}</p>
              </div>
            </div>
            <br>
        @endif
        <table id="data-table-simple" class="responsive-table display centered" cellspacing="0">
          <thead>
              <tr>
                  <th>Nama</th>
                  <th>Kategori</th>
                  <th>Status</th>
                  <th>Stok Minimum</th>
                  <th>Stok Sekarang</th>
                  <th></th>
              </tr>
          </thead>
          <tfoot>
              <tr>
                  <th>Nama</th>
                  <th>Kategori</th>
                  <th>Status</th>
                  <th>Stok Minimum</th>
                  <th>Stok Sekarang</th>
                  <th></th>
              </tr>
          </tfoot>
          <tbody>
            @foreach ($inventories as $inventory)
            <tr>
              <td>{{ $inventory->name }}</td>
              <td>{{ $inventory->category()->first()->name }}</td>
              <td>
                <!--
                    status convention:
                    0 - Kurang
                    1 - Cukup
                -->
                @if ($inventory->status == 0)
                  <span class="red-text">Kurang</span>
                @else
                  <span class="green-text">Cukup</span>
                @endif
              </td>
              <td>{{ $inventory->min_stock }} {{ $inventory->unit }}</td>
              <td>{{ $inventory->cards->last()->stock }} {{ $inventory->unit }}</td>
              <td>
                <a href="{{ route('card-detail', $inventory->id) }}" class="btn waves-effect waves-light light-blue darken-4 tooltipped" data-position="bottom" data-delay="1" data-tooltip="Kartu ATK"><i class="mdi-editor-insert-drive-file"></i></a>
                @if ($operator)
                  <a href="{{ route('inventory-update', $inventory->id) }}" class="btn waves-effect waves-light blue tooltipped" data-position="bottom" data-delay="1" data-tooltip="Ubah Data ATK"><i class="mdi-editor-border-color"></i></a> 
                  <a href="{{ route('inventory-delete', $inventory->id) }}" class="btn waves-effect waves-light light-blue darken-2 delete-inventory tooltipped" data-position="bottom" data-delay="1" data-tooltip="Hapus ATK"><i class="mdi-content-clear"></i></a>
                @endif
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('assets/js/plugins/data-tables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/data-tables/data-tables-script.js') }}"></script>
@endsection