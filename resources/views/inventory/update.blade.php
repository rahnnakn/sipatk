@extends('layouts.admin')
@section('page-title', 'Ubah ATK | ')
@section('title', 'Ubah ATK')
@section('root') 
  <li><a class="blue-text text-darken-4" href="{{ route('index') }}">Dashboard</a></li>
@endsection
@section('previous')
  <li><a class="blue-text text-darken-4" href="{{ route('inventory-index') }}">Daftar ATK</a></li>
@endsection
@section('here', 'Ubah ATK')
@if ($operator || $manager)
  @section('new_request', $count_requests)
@endif
@section('admin-content')
<div class="col s12 m8 l9">
	<div class="card">
		<div id="jqueryvalidation" class="section">
			<div class="container" id="inventory-create">
				<h4>Ubah ATK</h4>
				<br>
				<div id="card-alert" class="card blue darken-1">
					<div class="card-content white-text darken-1">
						<p>Area dengan (*) wajib diisi.</p>	
						@if (session()->has('flash_message'))
							<p class="single-alert">{{ session('flash_message') }}</p>
					    @endif
					</div>
	            </div>
				<form id="formValidate" class="row formValidate" method="post" action="{{ route('inventory-update', $inventory->id) }}">
					{{ csrf_field() }}
					<div class="col s12 m12 l12 validate">
						<label for="name">Nama ATK*</label>
						<div class="input-field">
							<input type="text" name="name" value="{{ $inventory->name }}" data-error=".errorName">
							<div class="errorName"></div>
					</div>
					</div>
					<div class="col s12 m4 l4 validate">
						<label for="min_stock">Stok Minimal*</label>
						<div class="input-field">
							<input type="text" name="min_stock" value="{{ $inventory->min_stock }}" data-error=".errorMinStock">
							<div class="errorMinStock"></div>
						</div>
					</div>
					<div class="col s12 m4 l4 validate">
						<label for="category">Pilih Kategori*</label>
						<div class="input-field">
							<select name="category" data-error=".errorCategory" required="required">
								@foreach ($cats as $cat)
									@if ($cat->id == $inventory->category)
										<option value="{{ $cat->id }}" selected>{{ $cat->name }}</option>
									@else
										<option value="{{ $cat->id }}">{{ $cat->name }}</option>
									@endif
								@endforeach
							</select>
							<div class="errorCategory"></div>
						</div>
					</div>
					<div class="col s12 m4 l4 unit">
						<label for="category">Pilih Satuan*</label>
						<div class="input-field">
							<select name="unit" data-error=".errorUnit" required="required">
							@foreach ($units as $unit)
								@if ($unit->unit == $inventory->unit)
									<option value="{{ $unit->unit }}" selected>{{ $unit->unit }}</option>
								@else
									<option value="{{ $unit->unit }}">{{ $unit->unit }}</option>
								@endif
							@endforeach
							</select>
							<div class="errorUnit"></div>
						</div>
						</div>
					<div class="col s12">
						<button class="btn waves-effect waves-light indigo darken-4 update-inventory">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script>
    $("#formValidate").validate({
        rules: {
            name: {
                required: true,
            },
           	min_stock: {
                required: true,
                digits: true,
            },
            category: {
	            required: true,
            },
            'unit[]': {
            	required: true,
            },
		},
        //For custom messages
        messages: {
            name:{
                required: "Nama ATK harus diisi",
            },
            min_stock:{
                required: "Stok minimal ATK harus diisi",
                digits: "Stok minimal harus berupa angka",
            },
            'unit[]': {
            	required: "Pilih satuan jumlah",
            },
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          if (placement) {
            $(placement).append(error)
          } else {
            error.insertAfter(element);
          }
        }
     });
</script>
@endsection

