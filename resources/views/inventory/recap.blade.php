<head>
    <style>
        td, th {
            border: 1px solid black;
        }
        .yellow {
            background: yellow;
        }
        .cyan {
            background: cyan;
        }
    </style>
</head>
<h5 style="margin-bottom:0;text-align:center">Rekapitulasi Penerimaan, Pengeluaran, dan Persediaan</h5>
<h6 style="margin-top:0;text-align:center">Alat Tulis Kantor (ATK) bagian AdTI <br> per TA.18 ({{ Carbon\Carbon::createFromFormat('Y-m-d', $transactions->first()->transaction_date)->format('d M Y') }} s/d {{ Carbon\Carbon::createFromFormat('Y-m-d', $transactions->last()->transaction_date)->format('d M Y') }})</h6>
<table style="width:100%;text-align:center;border-collapse:collapse">
    <tr>
        <th rowspan="2">No</th>
        <th rowspan="2">Nama Barang</th>
        <th class="yellow">Stok</th>
        <th colspan="2">Jan</th>
        <th colspan="2">Feb</th>
        <th colspan="2">Mar</th>
        <th colspan="2">Apr</th>
        <th colspan="2">Mei</th>
        <th colspan="2">Jun</th>
        <th colspan="2">Jul</th>
        <th colspan="2">Ags</th>
        <th colspan="2">Sep</th>
        <th colspan="2">Okt</th>
        <th colspan="2">Nov</th>
        <th colspan="2">Des</th>
        <th colspan="2" class="cyan">Jumlah</th>
        <th class="yellow">Stok</th>
    </tr>
    <tr>
        <th class="yellow">{{ Carbon\Carbon::createFromFormat('Y-m-d', $transactions->first()->transaction_date)->format('d M') }}</th>
        <th>M</th>
        <th>K</th>
        <th>M</th>
        <th>K</th>
        <th>M</th>
        <th>K</th>
        <th>M</th>
        <th>K</th>
        <th>M</th>
        <th>K</th>
        <th>M</th>
        <th>K</th>
        <th>M</th>
        <th>K</th>
        <th>M</th>
        <th>K</th>
        <th>M</th>
        <th>K</th>
        <th>M</th>
        <th>K</th>
        <th>M</th>
        <th>K</th>
        <th>M</th>
        <th>K</th>
        <th class="cyan">M</th>
        <th class="cyan">K</th>
        <th class="yellow">{{ Carbon\Carbon::createFromFormat('Y-m-d', $transactions->last()->transaction_date)->format('d M') }}</th>
    </tr>
    <?php $no = 1; ?>
    @foreach ($inventories as $inventory)
        <tr>
            <td>{{ $no }}</td>
            <td>{{ $inventory->name }}</td>
            <td class="yellow">
                @if(!is_null($inventories->find($no)->cards->first())) 
                    {{ $inventories->find($no)->cards->first()->stock }}
                @else
                    0
                @endif 
            </td>
            <!-- jan -->
            <td>{{ in_per_month(1, $no) ?: '' }}</td>
            <td>{{ out_per_month(1, $no) ?: '' }}</td>
            <!-- feb -->
            <td>{{ in_per_month(2, $no) ?: '' }}</td>
            <td>{{ out_per_month(2, $no) ?: '' }}</td>
            <!-- mar -->
            <td>{{ in_per_month(3, $no) ?: '' }}</td>
            <td>{{ out_per_month(3, $no) ?: '' }}</td>
            <!-- apr -->
            <td>{{ in_per_month(4, $no) ?: '' }}</td>
            <td>{{ out_per_month(4, $no) ?: '' }}</td>
            <!-- mei -->
            <td>{{ in_per_month(5, $no) ?: '' }}</td>
            <td>{{ out_per_month(5, $no) ?: '' }}</td>
            <!-- jun -->
            <td>{{ in_per_month(6, $no) ?: '' }}</td>
            <td>{{ out_per_month(6, $no) ?: '' }}</td>
            <!-- jul -->
            <td>{{ in_per_month(7, $no) ?: '' }}</td>
            <td>{{ out_per_month(7, $no) ?: '' }}</td>
            <!-- ags -->
            <td>{{ in_per_month(8, $no) ?: '' }}</td>
            <td>{{ out_per_month(8, $no) ?: '' }}</td>
            <!-- sep -->
            <td>{{ in_per_month(9, $no) ?: '' }}</td>
            <td>{{ out_per_month(9, $no) ?: '' }}</td>
            <!-- okt -->
            <td>{{ in_per_month(10, $no) ?: '' }}</td>
            <td>{{ out_per_month(10, $no) ?: '' }}</td>
            <!-- nov -->
            <td>{{ in_per_month(11, $no) ?: '' }}</td>
            <td>{{ out_per_month(11, $no) ?: '' }}</td>
            <!-- des -->
            <td>{{ in_per_month(12, $no) ?: '' }}</td>
            <td>{{ out_per_month(12, $no) ?: '' }}</td>
            <!-- jml -->
            <td class="cyan">{{ in_total($no) }}</td>
            <td class="cyan">{{ out_total($no) }}</td>
            <td class="yellow">
                @if(!is_null($inventories->find($no)->cards->last())) 
                    {{ $inventories->find($no)->cards->last()->stock }}
                @else
                    0
                @endif
            </td>
        </tr>
        <?php $no++; ?>
    @endforeach
</table>