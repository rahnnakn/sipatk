<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InventorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inventories')->insert([
            'name' => 'Bak Sampah',
            'min_stock' => '2',
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Bak Surat susun 2 Lg BI',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Bantalan Stempel Uk. Kecil No. 0',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Bantalan Stempel Uk. Besar',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Batu Baterai Kotak',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'kantong',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Batu Baterai Uk. A-2',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'kantong',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Batu Baterai Uk. A-3',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'kantong',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Belt Unit C 3210',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Belt Unit C 3300',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Buku Tulis Folio isi 100 lbr',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
        	'name' => 'Buku Tulis Folio isi 300 lbr',
        	'min_stock' => 2,
        	'status' => '0',
        	'category' => '1',
            'unit' => 'buah',
        	'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Buku Tulis Kwarto isi 100 lbr',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Carbon Folio',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
        	'name' => 'CD R',
        	'min_stock' => 2,
        	'status' => '0',
        	'category' => '1',
            'unit' => 'buah',
        	'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
        	'name' => 'CD RW',
        	'min_stock' => 2,
        	'status' => '0',
        	'category' => '1',
            'unit' => 'buah',
        	'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
                DB::table('inventories')->insert([
            'name' => 'DVD R',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'DVD RW',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
                DB::table('inventories')->insert([
            'name' => 'Clips Binder Merk Lion No.105',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'dus',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Clips Binder Merk Lion No.107',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'dus',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Clips Binder Merk Lion No.111',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'dus',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Clips Binder Merk Lion No.155',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'dus',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Clips Binder Merk Lion No.200',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'dus',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Clips Binder Merk Lion No.260',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'dus',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Correcting Fluid Tipp-Ex',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Daftar Pengantar Surat Antar Satker BI. 1334',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Daftar Dokumen Keluar',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Daftar Dokumen Masuk',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Daftar Isi Folder',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Daftar Rekapitulasi Warkat BI 014',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Dispenser Cellulose Tape',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Double Tape 1 Inc 3 M',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Drum Cartridge C 3210',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Drum Cartridge C 3300',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Drum Cartridge C 1618',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Drum Laser Printer Nec 870',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Flash Disk Sandisk',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Form. Penggantian Biaya Sakit KS.05',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Formulir DSDM 05.01',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Formulir DSDM 05.02',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Fuji Xerox Drum Phaser 4600 113R00762',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Fuji Xerox Toner Docuprint C3210 (Black)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Fuji Xerox Toner Docuprint C3210 (Cyan)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Fuji Xerox Toner Docuprint C3210 (Magenta)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Fuji Xerox Toner Docuprint C3210 (Yellow)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Fuji Xerox Toner Docuprint C3300 (Black)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Fuji Xerox Toner Docuprint C3300 (Cyan)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Fuji Xerox Toner Docuprint C3300 (Magenta)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Fuji Xerox Toner Docuprint C3300 (Yellow)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Fuji Xerox Toner Docuprint CP405D (Black)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Fuji Xerox Toner Docuprint CP405D (Cyan)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Fuji Xerox Toner Docuprint CP405D (Magenta)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Fuji Xerox Toner Docuprint CP405D (Yellow)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Fuji Xerox Toner Phaser 6700 (Black)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Fuji Xerox Toner Phaser 6700 (Cyan)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Fuji Xerox Toner Phaser 6700 (Magenta)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Fuji Xerox Toner Phaser 6700 (Yellow)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Fuser Docuprint C 3210',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Fuser Docuprint C 3300',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Gelas Minum BI',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Gunting',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Hechtmachine Max 10-R (Kecil)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Hechtmachine Max 3-R (Besar)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Isi Pisau Curter A-100/A-300 (K)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'kantong',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Isi Pisau Curter L-500 (B)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'kantong',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Kamper',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Karet Gelang',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'kantong',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Karet Penghapus Pelikan (merah-biru)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Karet Penghapus Steadler',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Karton Bufalo Skin (berbagai warna)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'lembar',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Kartu Persediaan Barang BI.1371',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Karung Besar Plastik',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'lembar',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Kertas Apli Micro',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'lembar',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Kertas Fax',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Kertas HVS 80 grm A-3',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'rim',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Kertas HVS 80 grm A-4',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'rim',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Kertas HVS 80 grm Folio',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'rim',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Kertas HVS 80 grm Kwarto',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'rim',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Kertas Mesin Hitung (besar, kecil)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'rim',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Kertas Surat 100 (A4B)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'rim',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Kertas Surat 100 (A4C) Tembusan',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'rim',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Kertas Surat 100 (A4B) Halaman',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'rim',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Kertas Surat 100 (A4B) Halaman Tembusan',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'rim',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Kertas Whiteboard',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Kotak Folder/Kardus Arsip Logo BI',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Kotak Majalah Logo BI',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Label Segera',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'pak',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Lak Segel Logo BI Merah (Rahasia)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'lembar',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Laser Jet 507 A (Black)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Laser Jet 507 A (Cyan)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Laser Jet 507 A (Magenta)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Laser Jet 507 A (Yellow)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Laser Jet Enterprise 500 M551DN (Black)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Laser Jet Enterprise 500 M551DN (Cyan)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Laser Jet Enterprise 500 M551DN (Magenta)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Laser Jet Enterprise 500 M551DN (Yellow)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Laser Pointer',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Lem Bondafix',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Lem Cair Merk Oglue',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Lem Stick',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Lembar Disposisi Dokumen',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Lembar Disposisi Pejabat',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Map Bundel Tempel',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Map Dokumen/Arsip Logo BI',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'lembar',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Map Folder Berbagai Warna (Kuping)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'lembar',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Map Gantung Folio Plastik Logo BI (Map Gantung)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Map MGR FL Plastik SNL Folio Bambi 4020',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Map Plastik Transparan Folio Bambi 4010',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Mark Notes',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Nomerator 6 Digit Merk Max N-607',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Ordner Folio Logo BI',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Paper Clip Merk Atom No. 303 (Trigonal)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'dus',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Pembatas 10 Warna Bantex',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'pak',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Pembolong/Perforator Kenko No. 30',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Pembolong/Perforator Kenko No. 85',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Pembuka Nieces/Remover',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Penghapus Mesin Tik Listrik Nakajima',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Penghapus Whiteboard',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Pisau Curter A-300 (Kecil)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Pisau Curter L-500 (Besar)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Pita Mesin Tik Nakajima',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Plakban Cellulose Tape 1/2 inc Merk Panfix',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Plakban Linen Lebar 2 inc',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Plakban Plastik 2 inc/pngk brng Bening',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Plakban Plastik 2 inc/pngk brng Coklat',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Plakban Cellulose Tape 1 inc Merk Panfix',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Post It Sign Here Merk 3M',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Post It Uk. 3x2 cm/No. 655',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Post It Uk. 3x3 cm/No. 656',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Post It Uk. 3x5 cm/No. 653',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Post It Uk. 3x5 cm/No. 654',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Ribon Epson LQ 2090',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Sabun Cair Sunlight',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Sampul Bank Pos KP-ST (1)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Sampul Coklat Uk. 11 x 31 cm',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'lembar',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Sampul Coklat Uk. 29,5 x 39,5 cm',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'lembar',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Sampul Coklat Uk. 13,5 x 29 cm',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'lembar',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Sampul Coklat Uk. 15 x 25 cm',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'lembar',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Sampul Coklat Uk. 17 x 27 cm',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'lembar',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Sampul Coklat Uk. 23 x 35 cm',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'lembar',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Sampul Coklat Uk. 32,5 x 43,5 cm',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'lembar',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Sampul Coklat Uk. 40 x 50 cm',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'lembar',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Sampul Kertas Minyak Uk. 11,5 x 27,5 cm',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'lembar',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Sampul Surat Berjendela',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'lembar',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Self Adhesive Label No. 101',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'kantong',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Self Adhesive Label No. 103',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'kantong',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Self Adhesive Label No. 104',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'kantong',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Self Adhesive Label No. 107',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'kantong',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Self Adhesive Label No. 109',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'kantong',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Self Adhesive Label No. 111',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'kantong',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Self Adhesive Label No. 121',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'kantong',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Spidol Artline 70 (Biru, Merah, Hitam, Hijau)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Spidol Kecil (Biru, Merah, Hitam, Hijau)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Spidol Kecil 12 Warna',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Spidol Whiteboard (Biru, Merah, Hitam, Hijau)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Stabilo (Hijau, Pink, Kuning, Biru, Orange)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Standar Penghalang Buku Dari Besi',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Stempel Tanggal Merk Trodat 1020',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Isi Steples Nieces No. 10-1 (Kecil)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'dus',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Isi Steples Nieces No. 3-1 (Besar)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'dus',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Surat Setoran BI 008',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Tinta Nomerator (Black)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Tinta Nomerator (Red)',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Tinta Printer HP 56 Black',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Tinta Printer HP 57 Tiga Warna',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Tinta Printer HP 95 Tiga Warna',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Tinta Stempel Violet Artline Ink',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Tinta Stempel Warna Violet Top',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Toner Fax Toshiba 500F',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Toner Fax E-Studio 170F Toshiba',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Toner HP 94',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Toner HP 95',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Transparancy Film Type 688 Clear',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'lembar',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Transfer Roll Cartridge C 3210',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Transfer Roll Cartridge C 3300',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Tutup Tatakan Gelas Melamin',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Warkat Kredit Transfer BI 003',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Warkat Pemindahbukuan BI 002',
            'min_stock' => 2,
            'status' => '0',
            'category' => '1',
            'unit' => 'buah',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
    }
}