<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InventoryCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inventory_categories')->insert([
            'name' => 'DLP',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventory_categories')->insert([
            'name' => 'Eproc',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
        DB::table('inventory_categories')->insert([
            'name' => 'Lainnya',
            'isShown' => 1,
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
        ]);
    }
}
