<?php

use Illuminate\Database\Seeder;

class UnitQuantitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('unit_quantities')->insert([
            'unit' => 'buah',
        ]);
        DB::table('unit_quantities')->insert([
            'unit' => 'lembar',
        ]);
        DB::table('unit_quantities')->insert([
            'unit' => 'dus',
        ]);
        DB::table('unit_quantities')->insert([
            'unit' => 'rim',
        ]);
        DB::table('unit_quantities')->insert([
            'unit' => 'kantong',
        ]);
        DB::table('unit_quantities')->insert([
            'unit' => 'pak',
        ]);
    }
}
