<?php

use Illuminate\Database\Seeder;

class DivisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('divisions')->insert([
            'name' => 'Arsitektur Enterprise',
        ]);
        DB::table('divisions')->insert([
            'name' => 'Strategi dan Transformasi Sistem Informasi',
        ]);
        DB::table('divisions')->insert([
            'name' => 'Manajemen Portofolio dan Program Sistem Informasi',
        ]);
        DB::table('divisions')->insert([
            'name' => 'Pengawasan Kualitas Sistem Informasi',
        ]);
        DB::table('divisions')->insert([
            'name' => 'Tim Konsultasi Pengaturan dan Standarisasi Sistem Informasi untuk Eksternal',
        ]);
        DB::table('divisions')->insert([
            'name' => 'Pengembangan dan Implementasi Aplikasi Manajemen Data',
        ]);
        DB::table('divisions')->insert([
            'name' => 'Pengembangan dan Implementasi Aplikasi Sektor Moneter',
        ]);
        DB::table('divisions')->insert([
            'name' => 'Pengembangan dan Implementasi Aplikasi Sektor Market',
        ]);
        DB::table('divisions')->insert([
            'name' => 'Pengembangan dan Implementasi Aplikasi Sektor SSK',
        ]);
        DB::table('divisions')->insert([
            'name' => 'Pengembangan dan Implementasi Aplikasi Sektor Sistem Pembayaran',
        ]);
        DB::table('divisions')->insert([
            'name' => 'Pengembangan dan Implementasi Aplikasi Sektor Manajemen Intern',
        ]);
        DB::table('divisions')->insert([
            'name' => 'Pengembangan dan Implementasi Teknologi dan Pengamanan Sistem Informasi',
        ]);
        DB::table('divisions')->insert([
            'name' => 'Sumber Daya Pengembangan Sistem Informasi',
        ]);
        DB::table('divisions')->insert([
            'name' => 'Pendukung Sistem Informasi',
        ]);
        DB::table('divisions')->insert([
            'name' => 'Operasional Sistem Informasi',
        ]);
        DB::table('divisions')->insert([
            'name' => 'Pengelolaan Aset dan Layanan Sistem Informasi',
        ]);
        DB::table('divisions')->insert([
            'name' => 'Manajemen Pelaksanaan Kontrak',
        ]);
        DB::table('divisions')->insert([
            'name' => 'Tim Perencanaan Pengadaan',
        ]);
        DB::table('divisions')->insert([
            'name' => 'Satuan Layanan dan Administrasi',
        ]);
        DB::table('divisions')->insert([
            'name' => 'Performance Manager',
        ]);
        DB::table('divisions')->insert([
            'name' => 'Internal Control Officer',
        ]);
    }
}