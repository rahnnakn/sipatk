<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBinInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bin_inventories', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->integer('id')->unsigned();
            $table->string('name');
            $table->integer('min_stock');
            $table->string('status');
            $table->integer('category');
            $table->string('unit');
            $table->timestamps();

            $table->foreign('id')
                    ->references('id')
                    ->on('inventories')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

            $table->index([
                'id', 
                'updated_at'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bin_inventories');
    }
}
