<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBinUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bin_users', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->string('username');
            $table->string('nip');
            $table->string('email');
            $table->string('name');
            $table->string('division');
            $table->boolean('isOperator');
            $table->boolean('isManager');
            $table->timestamps();

            $table->foreign('username')
                ->references('username')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->index([
                'username',
                'updated_at'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}