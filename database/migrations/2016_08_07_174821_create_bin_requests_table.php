<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBinRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bin_requests', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->integer('id')->unsigned();
            $table->string('author');
            $table->string('status');
            $table->text('purpose');
            $table->string('approver');
            $table->string('receiver')->nullable();
            $table->string('admin_note')->nullable();
            $table->string('ticket')->nullable();
            $table->boolean('approvedByDiv');
            $table->timestamps();

            $table->foreign('id')
                    ->references('id')
                    ->on('requests')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

            $table->index([
                'id', 
                'updated_at'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bin_requests');
    }
}
